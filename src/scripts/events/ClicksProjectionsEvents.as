package scripts.events
{
	import flash.events.Event;
	
	public class ClicksProjectionsEvents extends Event
	{
		private var _data:Object;
		public static var DISPLAY_CLICKS_PROJECTIONS:String = "displayClicksProjections";
		public static var CALCULATE_RATE:String = "calculateRate";
		public static var CALCULATE_CLICKS:String = "calculateClicks";
		
		
		/**
		 * Event class constructor.
		 * @param type (String)
		 * @param bubbles (Boolean)
		 * @param cancelable (Boolean)
		 * @param data (Object)
		 * @return none
		 * @author Jerusalem Alvaira
		 */
		public function ClicksProjectionsEvents(type:String, bubbles:Boolean=false, cancelable:Boolean=false, data:Object = null)
		{
			super(type, bubbles, cancelable);
			
			_data = data;
		}
		
		/**
		 * Class method overide.
		 * @param none
		 * @return Event
		 * @author Jerusalem Alvaira 
		 */
		override public function clone():Event {
			return new ClicksProjectionsEvents(type, bubbles, cancelable, data);
		}
		
		public function get data():Object {
			return _data;
		}
	}
}