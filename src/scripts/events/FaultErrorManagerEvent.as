package events {
	import flash.events.Event;
	
	public class FaultErrorManagerEvent extends Event {
		
		private var _data:Object;		
		public static var TOGGLE_FAULT_ERROR:String = "toggleFaultError";
		public static var UPDATE_RCOPIA_ERROR_LOGS:String = "updateDrFirstErrorLogs";
		
		public function FaultErrorManagerEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false, data:Object = null) {
			super(type, bubbles, cancelable);
			_data = data;
		}
		
		override public function clone():Event {
			return new FaultErrorManagerEvent(type, bubbles, cancelable, data);
		}
		
		
		public function get data():Object {
			return _data;
		}
	}
}