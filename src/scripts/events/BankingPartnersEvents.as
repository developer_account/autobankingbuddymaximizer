package scripts.events
{
	import flash.events.Event;
	
	public class BankingPartnersEvents extends Event
	{
		private var _data:Object;
		public static var DISPLAY_BANKING_PARTNERS_LIST:String = "displayBankingPartnersList";
		public static var DISPLAY_RECEIVING_BANKING_PARTNERS:String = "displayReceivingBankingPartners";
		public static var DISPLAY_SENDING_BANKING_PARTNERS:String = "displaySendingBankingPartners";
		public static var DISPLAY_TOP_RECEIVING_BANKING_PARTNERS:String = "displayTopReceivingBankingPartners";
		public static var DISPLAY_TOP_SENDING_BANKING_PARTNERS:String = "displayTopSendingBankingPartners";
		public static var ADD_BANKING_PARTNER_RESULT:String = "addBankingPartnerResult";
		public static var DELETE_BANKING_PARTNER_RESULT:String = "deleteBankingPartnerResult";
		public static var DISPLAY_CHART_DATA:String = "displayChartData";
		public static var DISPLAY_RECEIVING_CHART_DATA:String = "displayReceivingChartData";
		public static var DISPLAY_SENDING_CHART_DATA:String = "displaySendingChartData";
		
		
		/**
		 * Event class constructor.
		 * @param type (String)
		 * @param bubbles (Boolean)
		 * @param cancelable (Boolean)
		 * @param data (Object)
		 * @return none
		 * @author Jerusalem Alvaira
		 */
		public function BankingPartnersEvents(type:String, bubbles:Boolean=false, cancelable:Boolean=false, data:Object = null)
		{
			super(type, bubbles, cancelable);
			
			_data = data;
		}
		
		/**
		 * Class method overide.
		 * @param none
		 * @return Event
		 * @author Jerusalem Alvaira 
		 */
		override public function clone():Event {
			return new BankingPartnersEvents(type, bubbles, cancelable, data);
		}
		
		public function get data():Object {
			return _data;
		}
	}
}