package scripts.events
{
	import flash.events.Event;

	public class ComponentEvents extends Event
	{
		
		private var _data:Object;
		public static var NOTIFY:String = 'notify';
		
		public function ComponentEvents(type:String, bubbles:Boolean=false, cancelable:Boolean=false, data:Object = null)
		{
			super(type, bubbles, cancelable);
			_data = data;
		}
		
		/**
		 * Class method overide.
		 * @param none
		 * @return Event
		 * @author Jerusalem Alvaira 
		 */
		override public function clone():Event {
			return new BankingPartnersEvents(type, bubbles, cancelable, data);
		}
		
		public function get data():Object {
			return _data;
		}
	}
}