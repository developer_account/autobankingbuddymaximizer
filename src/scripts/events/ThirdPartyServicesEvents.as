package scripts.events
{
	import flash.events.Event;
	
	public class ThirdPartyServicesEvents extends Event
	{
		private var _data:Object;
		public static var DISPLAY_THIRD_PARTY_URLS:String = "displayThirdPartyUrls";
		public static var LOAD_LOGIN:String = "loadLogin";
		public static var DISPLAY_QCC_VALUES:String = "displayQCCValues";
		public static var DISPLAY_CM_VALUES:String = "displayCMValues";
		public static var DISPLAY_THIRD_PARTY_VALUES:String = "displayThirdPartyValues";
		public static var DISPLAY_CB_VALUES:String = "displayCBValues";
		
		/**
		 * Event class constructor.
		 * @param type (String)
		 * @param bubbles (Boolean)
		 * @param cancelable (Boolean)
		 * @param data (Object)
		 * @return none
		 * @author Jerusalem Alvaira
		 */
		public function ThirdPartyServicesEvents(type:String, bubbles:Boolean=false, cancelable:Boolean=false, data:Object = null)
		{
			super(type, bubbles, cancelable);
			
			_data = data;
		}
		
		/**
		 * Class method overide.
		 * @param none
		 * @return Event
		 * @author Jerusalem Alvaira 
		 */
		override public function clone():Event {
			return new BankingPartnersEvents(type, bubbles, cancelable, data);
		}
		
		public function get data():Object {
			return _data;
		}
	}
}