package scripts.events
{
	import flash.events.Event;
	
	public class LoginEvents extends Event
	{
		private var _data:Object;
		public static var LOGIN_SUCCESS:String = 'loginSuccess';
		public static var GET_USER_COUNT_RESULT:String = 'getUserCountResult';
		public static var REGISTRATION_SUCCESS:String = 'registrationSuccess';
		public static var GET_USER_COUNT_SQL_RESULT:String = 'getUserCountSQLResult';
		public static var REGISTER_USER:String = 'registerUser';
		
		public function LoginEvents(type:String, bubbles:Boolean=false, cancelable:Boolean=false, data:Object = null)
		{
			super(type, bubbles, cancelable);
			_data = data;
		}
		
		/**
		 * Class method overide.
		 * @param none
		 * @return Event
		 * @author Jerusalem Alvaira 
		 */
		override public function clone():Event {
			return new LoginEvents(type, bubbles, cancelable, data);
		}
		
		public function get data():Object {
			return _data;
		}
	}
}