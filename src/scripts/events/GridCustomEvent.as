package scripts.events 
{ 
	import flash.events.Event;

	public class GridCustomEvent extends Event {
		
		public static var EDIT_BANKING_PARTNER:String = "editBankingPartner";
		
		private var _data:Object;
		
		public function GridCustomEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false, data:Object=null) {
			super(type, bubbles, cancelable);
			_data = data		
		}
		
		override public function clone():Event {
			return new GridCustomEvent(type, bubbles, cancelable, data);
		}
		
		public function get data():Object {
			return _data;
		}
	}
}