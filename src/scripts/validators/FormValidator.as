package scripts.validators
{
	import mx.validators.DateValidator;
	import mx.validators.EmailValidator;
	import mx.validators.StringValidator;
	import mx.validators.Validator;
	
	import spark.validators.NumberValidator;
	
	public class FormValidator
	{
		private var textValidator:StringValidator;
		private var numberValidator:NumberValidator;
		private var emailValidator:EmailValidator;
		private var dropDownValidator:Validator;
		private var dateValidator:DateValidator;
		private var invalidFieldList:Array;
		
		
		public function FormValidator() {
			textValidator = new StringValidator();
			emailValidator = new EmailValidator();
			dropDownValidator = new Validator();
			numberValidator = new NumberValidator();
			dateValidator = new DateValidator();
		}
		
		/**
		 * Validates the specified fields.
		 * @param fields (Array)
		 * 
		 * 
		 */
		public function validate(fields:Array):Boolean {
			var inner:Object;
			var validCount:int = 0;
			invalidFieldList = new Array();
			for(var i:int = 0; i < fields.length; i++) {
				inner = fields[i];
				if(inner.source.errorString != null) {
					inner.source.errorString = '';
				}				
				switch(String(inner.type)) {
					case 'text': {
						textValidator.source = inner.source;
						textValidator.property = inner.property;
						textValidator.required = Boolean(inner.required);							
						if(inner.minLength != null) { textValidator.minLength = inner.minLength; }
						if(inner.maxLength != null) { textValidator.maxLength = inner.minLength; }
						if(inner.tooLongError != null) { textValidator.tooLongError = inner.tooLongError; }
						if(inner.tooShortError != null) { textValidator.tooShortError = inner.tooShortError; }
						if(inner.requiredFieldError != null) { textValidator.requiredFieldError = inner.requiredFieldError;	}
						
						if(textValidator.validate().type == 'valid') {
							validCount++;
						} else {
							invalidFieldList.push(inner.source);
						}
						break;
					}
						
					case 'email': {
						emailValidator.source = inner.source;
						emailValidator.property = inner.property;
						emailValidator.required = Boolean(inner.required);
						if(emailValidator.validate().type == 'valid') {
							validCount++;
						} else {
							invalidFieldList.push(inner.source);
						}
						break;
					}
						
					case 'number': {
						numberValidator.source = inner.source;
						numberValidator.property = inner.property;
						numberValidator.required = Boolean(inner.required);
						if(inner.minValue != null) { numberValidator.minValue = inner.minValue; }
						if(inner.maxValue != null) { numberValidator.maxValue = inner.maxValue; }
						if(inner.requiredFieldError != null) { numberValidator.requiredFieldError = inner.requiredFieldError; }
						if(inner.lessThanMinError != null) { numberValidator.lessThanMinError = inner.lessThanMinError;	}
						if(inner.greaterThanMaxError != null) { numberValidator.greaterThanMaxError = inner.greaterThanMaxError; }
						if(numberValidator.validate().type == 'valid') {
							validCount++;
						} else {
							invalidFieldList.push(inner.source);
						}
						break;
					}
						
					case 'date': {
						dateValidator.source = inner.source;
						dateValidator.property = inner.property;
						dateValidator.required = Boolean(inner.required);
						if(inner.requiredFieldError != null) { dateValidator.requiredFieldError = inner.requiredFieldError; }
						if(dateValidator.validate().type == 'valid') {
							validCount++;
						} else {
							invalidFieldList.push(inner.source);
						}
						
						break;
					}
						
					case 'dropdown': {
						dropDownValidator.source = inner.source;
						dropDownValidator.property = inner.property;
						dropDownValidator.required = Boolean(inner.required);
						if(inner.requiredFieldError != null) { dropDownValidator.requiredFieldError = inner.requiredFieldError; }
						if(dropDownValidator.validate().type == 'valid') {
							validCount++;
						} else {
							invalidFieldList.push(inner.source);
						}
						break;
					}
				}
				
			}	
			
			if(invalidFieldList.length > 0) {
				invalidFieldList[0].setFocus();
			}
			
			return (validCount == fields.length) ? true : false;
		}
	}
}