package scripts.lib {
	import flash.data.EncryptedLocalStore;
	import flash.utils.ByteArray;

	public class SessionManager	{
		
		import flash.filesystem.File;
		import flash.filesystem.FileMode;
		import flash.filesystem.FileStream;
		import mx.controls.Alert;
		import mx.utils.ObjectUtil;
		
		public var file:File;
		
		public function SessionManager() {
			file = File.applicationStorageDirectory.resolvePath("session.xml");						
		}
		
		public function setSession(data:Object):void {
			var newXMLStr:String = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
			
			this.setSessionObjects(data);
			
			newXMLStr += "<session>";
			newXMLStr += "<user>" + data.user_name + "</user>";
			newXMLStr += "<user_id>" + data.user_id + "</user_id>";
			newXMLStr += "</session>";
			
			var fs:FileStream = new FileStream();
			fs.open(file, FileMode.WRITE);
			fs.writeUTFBytes(newXMLStr);
			fs.close();
		}
		
		public function getSession():XML {			
			var fileStream:FileStream = new FileStream();
			fileStream.open(file, FileMode.READ);
			var prefsXML:XML = XML(fileStream.readUTFBytes(fileStream.bytesAvailable));
			fileStream.close();
			return prefsXML;
		}
		
		protected function setSessionObjects(data:Object):void {
			var bytes:ByteArray = new ByteArray();
			bytes.writeObject(data);
			EncryptedLocalStore.setItem("AutoBankingBuddyMaximizer.user.session", bytes);
		}
		
		public function getSessionObjects():Object {
			var bytes:ByteArray = EncryptedLocalStore.getItem("AutoBankingBuddyMaximizer.user.session");
			var sessions:Object = new Object();
			if(bytes) {
				sessions = bytes.readObject();
			}
			
			return sessions;
		}
		
		public function deleteSession():void {
			file.deleteFile();
		}
		
	}
}