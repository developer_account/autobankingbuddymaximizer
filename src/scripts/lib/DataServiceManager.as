package scripts.lib
{
	//import events.FaultErrorManagerEvent;
	
	import flash.events.EventDispatcher;
	
	import mx.rpc.events.FaultEvent;
	import mx.rpc.http.HTTPService;
	import mx.rpc.remoting.mxml.RemoteObject;
	
	import scripts.lib.ConfigurationsManager;
	
	public class DataServiceManager extends EventDispatcher {
		private var cm:ConfigurationsManager;
		private var config:Object;
		
		public function DataServiceManager() {
			cm = new ConfigurationsManager();
			config = cm.getConfigurations();
		}
		
		public function create(source:String, showBusyCursor:Boolean = true):RemoteObject {
			
			var service:RemoteObject = new RemoteObject();
			
			service.destination = String(config.destination);
			service.source = source;
			service.endpoint = String(config.endpoint);
			service.showBusyCursor = showBusyCursor;
			service.addEventListener(FaultEvent.FAULT, faultEventHandler);
			
			return service;
			
		}
		
		protected function faultEventHandler(event:FaultEvent):void {
			//dispatchEvent(new FaultErrorManagerEvent(FaultErrorManagerEvent.TOGGLE_FAULT_ERROR, true, false, null));
		}
		
		public function destroy(service:RemoteObject):void {
			service = null;
			service = null;
		}
		
		public function createLicense(source:String, showBusyCursor:Boolean = true):RemoteObject
		{
			var service:RemoteObject = new RemoteObject();
			
			service.destination = String(config.destination);
			service.source = source;
			service.endpoint = String(config.licensingEndpoint);
			service.showBusyCursor = showBusyCursor;
			service.addEventListener(FaultEvent.FAULT, faultEventHandler);
			
			return service;
		}
	}
}