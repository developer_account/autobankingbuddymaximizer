package scripts.lib
{
	import flash.data.SQLCollationType;
	import flash.data.SQLConnection;
	import flash.data.SQLResult;
	import flash.data.SQLStatement;
	import flash.errors.SQLError;
	import flash.events.SQLErrorEvent;
	import flash.events.SQLEvent;
	import flash.filesystem.File;

	/**
	 * Name:
	 * Description:
	 * Author: 
	 */
	public class DatabaseManager
	{
		include "../../scripts/formatters/GlobalFormatter.as";
		
		private var connection:SQLConnection;
		private var queryStmt:SQLStatement;
		private var queryTxt:String;
		public function DatabaseManager()
		{
			initializeDatabaseConnection();
		}
		
		private function initializeDatabaseConnection():void
		{
			
		}
		
		public function openDatabase():SQLConnection
		{
			var dbFile:File = File.applicationStorageDirectory.resolvePath("AutoBankingBuddyMaximizer.db");
			var conn:SQLConnection = new SQLConnection();
			connection = new SQLConnection();
			connection.addEventListener(SQLEvent.OPEN, onOpenSuccess);
			connection.addEventListener(SQLErrorEvent.ERROR, openFailure);
			connection.open(dbFile);
			
			return connection;
			
			//connection.openAsync(dbFile, SQLMode.CREATE);
		}
		
		protected function openFailure(event:SQLErrorEvent):void
		{
			// TODO Auto-generated method stub
			
		}
		
		protected function onOpenSuccess(event:SQLEvent):void
		{
		}
		
		public function createTables():void
		{
			createBankingPartnersTable();
			createClicksProjectionTable();
			createLogsTable();
			createThirdPartyUrlsTable();
			createUserProfilesTable();
			createUserTypesTable();
			createUsersTable();
			
		}
		
		private function createUsersTable():void
		{
			queryStmt = new SQLStatement();
			
			queryStmt.sqlConnection = connection;
						
			/*queryTxt = "DROP TABLE IF EXISTS users";
			queryStmt.text = queryTxt;
			queryStmt.execute();*/
			
			queryTxt = "CREATE TABLE IF NOT EXISTS users (" +
				"id	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
				"user_type_id INTEGER NOT NULL," +
				"user_name	TEXT NULL," +
				"password	TEXT NULL," +
				"license	TEXT NOT NULL," +
				"verified_license	INTEGER NOT NULL DEFAULT 0," + 
				"deleted	INTEGER NOT NULL," +
				"created_by	INTEGER NOT NULL," +
				"modified_by	INTEGER NOT NULL," +
				"date_created	TEXT NOT NULL," +
				"date_modified	TEXT NOT NULL" +
				");";
			
			queryStmt.text = queryTxt;
			
			queryStmt.addEventListener(SQLEvent.RESULT, createTableResult);
			queryStmt.addEventListener(SQLErrorEvent.ERROR, createTableError);
			queryStmt.execute();
		}
		
		private function createUserTypesTable():void
		{
			queryStmt = new SQLStatement();
			
			queryStmt.sqlConnection = connection;
			
			queryTxt = "CREATE TABLE IF NOT EXISTS user_types (" +
				"id	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
				"type TEXT NOT NULL," +
				"description	TEXT NOT NULL," +
				"deleted	INTEGER NOT NULL," +
				"created_by	INTEGER NOT NULL," +
				"modified_by	INTEGER NOT NULL," +
				"date_created	TEXT NOT NULL," +
				"date_modified	TEXT NOT NULL" +
				");";
			
			queryStmt.text = queryTxt;
			
			queryStmt.addEventListener(SQLEvent.RESULT, createTableResult);
			queryStmt.addEventListener(SQLErrorEvent.ERROR, createTableError);
			queryStmt.execute();
		
		}
		
		private function createUserProfilesTable():void
		{
			queryStmt = new SQLStatement();
			
			queryStmt.sqlConnection = connection;
			
			queryTxt = "CREATE TABLE IF NOT EXISTS user_profiles (" +
				"id	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
				"user_id INTEGER NOT NULL," +
				"title TEXT NULL," +
				"first_name TEXT NULL," +
				"middle_name TEXT NULL," +
				"last_name TEXT NULL," +
				"suffix TEXT NULL," +
				"address1 TEXT NULL," +
				"address2 TEXT NULL," +
				"city TEXT NULL," +
				"state TEXT NULL," +
				"zip TEXT NULL," +
				"country TEXT NULL," +
				"phone TEXT NULL," +
				"mobile TEXT NULL," +
				"email TEXT NULL," +
				"deleted	INTEGER NOT NULL," +
				"created_by	INTEGER NOT NULL," +
				"modified_by	INTEGER NOT NULL," +
				"date_created	TEXT NOT NULL," +
				"date_modified	TEXT NOT NULL" +
				");";
			
			queryStmt.text = queryTxt;
			
			queryStmt.addEventListener(SQLEvent.RESULT, createTableResult);
			queryStmt.addEventListener(SQLErrorEvent.ERROR, createTableError);
			queryStmt.execute();
		}
		
		private function createThirdPartyUrlsTable():void
		{
			queryStmt = new SQLStatement();
			
			queryStmt.sqlConnection = connection;
			
			queryTxt = "DROP TABLE IF EXISTS third_party_urls";
			queryStmt.text = queryTxt;
			queryStmt.execute();
			
			queryTxt = "CREATE TABLE IF NOT EXISTS third_party_urls (" +
				"id	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
				"name	TEXT NOT NULL," +
				"description	TEXT NOT NULL," +
				"deleted	INTEGER NOT NULL," +
				"created_by	INTEGER NOT NULL," +
				"modified_by	INTEGER NOT NULL," +
				"date_created	TEXT NOT NULL," +
				"date_modified	TEXT NOT NULL" +
				");";
			
			queryStmt.text = queryTxt;
			
			queryStmt.addEventListener(SQLEvent.RESULT, createThirdPartyTableResult);
			queryStmt.addEventListener(SQLErrorEvent.ERROR, createThirdPartyTableError);
			queryStmt.execute();
		}
		
		
		protected function createThirdPartyTableResult(event:SQLEvent):void
		{
			queryStmt = new SQLStatement();
			
			queryStmt.sqlConnection = connection;
			var sql:String = "INSERT OR REPLACE INTO 'third_party_urls' (id, name, description, deleted, created_by, modified_by, date_created, date_modified) ";
			sql +=  "SELECT '1' as 'id', 'ClickMagick' as 'name', 'http://www.clickmagick.com/share/1304119214' as 'description', 0 as 'deleted', 1 as 'created_by', 1 as 'modified_by', '"+getCurrentDate()+"' as 'date_created', '"+getCurrentDate()+"' as 'date_modified'" +
				" UNION SELECT '2' as 'id', 'QCC', 'http://product-click.com//view_stats.php?stats=ViObvwod78druN8kstYNVmP%2BuTTJrb4Fehce3qqV6g4%3D', 0, 1, 1, '"+getCurrentDate()+"', '"+getCurrentDate()+"'" +
				" UNION SELECT '3' as 'id', 'ClickBoss', 'http://instantlyprofitonline.com/stats.php?i=119&p=Rambo', 0, 1, 1, '"+getCurrentDate()+"', '"+getCurrentDate()+"';";
			/*sql += "VALUES ('ClickMagick', 'http://www.clickmagick.com/share/1304119214', 0, 1, 1, '"+getCurrentDate()+"', '"+getCurrentDate()+"')," +
				" ('QCC', 'http://product-click.com//view_stats.php?stats=ViObvwod78druN8kstYNVmP%2BuTTJrb4Fehce3qqV6g4%3D', 0, 1, 1, '"+getCurrentDate()+"', '"+getCurrentDate()+"');"; +
				" ('ClickBoss', 'http://instantlyprofitonline.com/stats.php?i=119&p=Rambo', 0, 1, 1, '"+getCurrentDate()+"', '"+getCurrentDate()+"');";*/
			
			queryStmt.text = sql;
			
			queryStmt.addEventListener(SQLEvent.RESULT, createTableResult);
			queryStmt.addEventListener(SQLErrorEvent.ERROR, createTableError);
			queryStmt.execute();
		}
		
		protected function createThirdPartyTableError(event:SQLErrorEvent):void
		{
			
		}
		
		private function createLogsTable():void
		{
			queryStmt = new SQLStatement();
			
			queryStmt.sqlConnection = connection;
			
			queryTxt = "CREATE TABLE IF NOT EXISTS  logs (" +
				"id	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
				"action	TEXT NULL," +
				"identifier_id	INTEGER NOT NULL," +
				"identifier_table	TEXT NOT NULL," +
				"deleted	INTEGER NOT NULL," +
				"created_by	INTEGER NOT NULL," +
				"modified_by	INTEGER NOT NULL," +
				"date_created	TEXT NOT NULL," +
				"date_modified	TEXT NOT NULL" +
				");";
			
			queryStmt.text = queryTxt;
			
			queryStmt.addEventListener(SQLEvent.RESULT, createTableResult);
			queryStmt.addEventListener(SQLErrorEvent.ERROR, createTableError);
			queryStmt.execute();
		}
		
		private function createBankingPartnersTable():void
		{
			queryStmt = new SQLStatement();
			
			queryStmt.sqlConnection = connection;
			
			queryTxt = "CREATE TABLE IF NOT EXISTS banking_partners (" +
				"id	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
				"user_profile_id	INTEGER NOT NULL," +
				"sent_first	INTEGER NOT NULL," +
				"receiving	INTEGER NOT NULL," +
				"number_of_rounds	INTEGER NOT NULL," +
				"clicks_owed	INTEGER NOT NULL," +
				"third_party_url_id	INTEGER NOT NULL," +
				"third_party_url	TEXT NOT NULL," +
				"total_clicks	INTEGER NOT NULL," +
				"unique_clicks	TEXT NULL," +
				"subscribers_received	INTEGER NULL," +
				"clicks_received	INTEGER NOT NULL DEFAULT 0," +
				"status	INTEGER NOT NULL DEFAULT 0," +
				"date_started	TEXT NOT NULL," +
				"deleted	INTEGER NOT NULL," +
				"created_by	INTEGER NOT NULL," +
				"modified_by	INTEGER NOT NULL," +
				"date_created	TEXT NOT NULL," +
				"date_modified	TEXT NOT NULL" +
				");"
			
			queryStmt.text = queryTxt;
			
			queryStmt.addEventListener(SQLEvent.RESULT, createTableResult);
			queryStmt.addEventListener(SQLErrorEvent.ERROR, createTableError);
			queryStmt.execute();
			
		}
		
		private function createClicksProjectionTable():void
		{
			queryStmt = new SQLStatement();
			
			queryStmt.sqlConnection = connection;
			
			queryTxt = "CREATE TABLE IF NOT EXISTS clicks_projection (" +
				"id	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
				"average_opt_in_rate	TEXT NOT NULL," +
				"clicks_coming_in	INTEGER NOT NULL," +
				"deleted	INTEGER NOT NULL," +
				"created_by	INTEGER NOT NULL," +
				"modified_by	INTEGER NOT NULL," +
				"date_created	TEXT NOT NULL," +
				"date_modified	TEXT NOT NULL" +
				");";
			
			queryStmt.text = queryTxt;
			
			queryStmt.addEventListener(SQLEvent.RESULT, createTableResult);
			queryStmt.addEventListener(SQLErrorEvent.ERROR, createTableError);
			queryStmt.execute();
		}		
		
		protected function createTableResult(event:SQLEvent):void
		{
			
		}
		
		protected function createTableError(event:SQLErrorEvent):void
		{
			trace(event.error.details);
			trace(event.error.message);
			//trace(event.error.);
		}
		
		
		
	}
}