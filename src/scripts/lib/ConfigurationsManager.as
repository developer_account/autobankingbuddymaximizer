package scripts.lib
{
	import flash.data.EncryptedLocalStore;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.text.ReturnKeyLabel;
	import flash.utils.ByteArray;
	
	import mx.controls.Alert;
	import mx.utils.ObjectUtil;
	
	/**
	 * Name:
	 * Description:
	 * Author: 
	 */
	public class ConfigurationsManager {
		
		private var file:File = File.applicationStorageDirectory.resolvePath("configurations.xml");
		private var file2:File = File.applicationStorageDirectory.resolvePath("login.xml");
		
		public var newXMLStr:String =new String();
		public var prefsXML:XML; 
		
		//[Bindable]public var haltSendDataValue:String;
		
		[Bindable]
		public var loginAttempts:String;
		[Bindable]
		public var loginPeriod:String;
		[Bindable]
		public var loginIdle:String;
		
		
		public function ConfigurationsManager() {
			/*if(!file.exists) {*/ createConfigurationFile(file);
		}
		
		private function createConfigurationFile(file:File):void {
			/*<endpoint>http://local.dev.bankingswapproject.com/bankingsoftware/</endpoint>*/
			var xml:XML = <configurations>
							  <endpoint>http://onlinetrainingleverage.com/bankingsoftware/</endpoint>
   							  <licensingEndpoint>http://gears.mech-marketing.com/licensing/</licensingEndpoint>
							  <updateURL>http://onlinetrainingleverage.com/bankingsoftware/updater/update.xml</updateURL>
							  <destination>amfphpGateway</destination>
							  <!--<endpoint>http://lofty-cabinet-743.appspot.com/</endpoint>-->
                              <!--<endpoint>http://local.dev.bankingswapproject.com/bankingsoftware/</endpoint>-->
							  <!--<endpoint>http://localhost:8080</endpoint>-->
							</configurations>
			var fs:FileStream = new FileStream();
			fs.open(file, FileMode.WRITE);
			fs.writeUTFBytes("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" + xml.toString());
			fs.close();
		}
		
		public function getConfigurations():XML {
			var fileStream:FileStream = new FileStream();
			fileStream.open(file, FileMode.READ);
			prefsXML = XML(fileStream.readUTFBytes(fileStream.bytesAvailable));
			fileStream.close();
			return prefsXML;
		}
		
		/**
		 * @name: createLoginFile
		 * @desc:
		 * @author: MM
		 */ 
		public function createLoginFile(file2:File):void {
			newXMLStr = "" +
				"<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
				"\n<login>" +
				"\n\t\t<loginAttempts>" + loginAttempts + "</loginAttempts>" +
				"\n\t\t<loginPeriod>" + loginPeriod + "</loginPeriod>" +
				"\n\t\t<loginIdle>" + loginIdle + "</loginIdle>" +
				"\n</login>\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n";
			
			var fs:FileStream = new FileStream();
			fs.open(file2, FileMode.WRITE);
			fs.writeUTFBytes(newXMLStr);
			fs.close();
		}
		
		/**
		 * @name: editLoginFile
		 * @desc:
		 * @author: MM
		 */ 
		public function editLoginFile():XML {
			newXMLStr = "" +
				"<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
				"\n<login>" +
				"\n\t\t<loginAttempts>" + loginAttempts + "</loginAttempts>" +
				"\n\t\t<loginPeriod>" + loginPeriod + "</loginPeriod>" +
				"\n</login>";
			
			var fs:FileStream = new FileStream();
			fs.open(file2, FileMode.UPDATE);
			fs.position =int(newXMLStr.indexOf("<login>"));
			fs.writeUTFBytes("" +
				"<login>" +
				"\n\t\t<loginAttempts>" + loginAttempts +"</loginAttempts>" +
				"\n\t\t<loginPeriod>" + loginPeriod + "</loginPeriod>" +
				"\n\t\t<loginIdle>" + loginIdle + "</loginIdle>" +
				"\n</login>\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
			fs.close();
			
			return getLoginConfig();
		}
		
		public function getLoginConfig():XML {
			var fileStream:FileStream = new FileStream();
			fileStream.open(file2, FileMode.READ);
			prefsXML =XML(fileStream.readUTFBytes(fileStream.bytesAvailable));
			fileStream.close();
			return prefsXML;
		}
		
		public function loginAttempSession(data:Object, key:String):void {
			var bytes:ByteArray = new ByteArray();
			bytes.writeObject(data);
			EncryptedLocalStore.setItem(key, bytes);
		}
		
		public function getLoginAttempSession(key:String):Object {
			var bytes:ByteArray = EncryptedLocalStore.getItem(key);
			var sessions:Object = new Object();
			if(bytes) {
				sessions = bytes.readObject();
			}			
			return sessions;
		}
		
		public function resetSession():void {
			EncryptedLocalStore.reset();
		}
		
		public function removeItemSession(key:String):void {
			EncryptedLocalStore.removeItem(key);
		}
		
		
		
	}
}