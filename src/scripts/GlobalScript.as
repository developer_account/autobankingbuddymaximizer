import flash.data.SQLConnection;

import mx.collections.ArrayCollection;

import scripts.lib.ConfigurationsManager;
import scripts.lib.DatabaseManager;
import scripts.lib.SessionManager;

[Bindable]private var config:XML = new XML();
private var cm:ConfigurationsManager;

private var dbm:DatabaseManager;
[Bindable] private var dbConfig:Object = new Object();
[Bindable] private var conn:SQLConnection = new SQLConnection();

private var sm:SessionManager;
[Bindable]private var session:Object = new Object();

public function initializeConfigurations():void{
	cm = new ConfigurationsManager();
	config = cm.getConfigurations();
}

public function initializeDatabase():void{
	
	dbm = new DatabaseManager();
	conn = dbm.openDatabase();
	//dbConfig = dbm.openDatabase();
	
}

public function initializeDatabaseTable():void{
	dbm.createTables();
}

public function initializeSession():void{
	sm = new SessionManager();
	session = sm.getSessionObjects();
}