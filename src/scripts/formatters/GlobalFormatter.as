/**test
 * 
*/
import mx.collections.ArrayCollection;
import mx.formatters.DateFormatter;

public function setTempArray(data:ArrayCollection, screen:String):ArrayCollection{
	
	var returnValue:ArrayCollection = new ArrayCollection();
	var temp:Object = new Object();
	var inner:Object;
	
	if(screen == 'home'){
		for(var j:String in data){
			inner = new Object();
			
			temp = data[j];
			
			inner = {
				receiving : temp.fullDetails.receiving,
				banking_partner_id : temp.fullDetails.banking_partner_id,
				banking_partner_name : temp.fullDetails.banking_partner_name,
				clicks_owed : temp.fullDetails.clicks_owed,
				total_clicks : temp.fullDetails.total_clicks,
				is_total : temp.fullDetails.is_total,
				date_started : (temp.date_started != null) ? temp.date_started : ""
			};
			
			returnValue.addItem(inner);
		}
	}else{
		
		for(var i:String in data){
			
			inner = new Object();
			
			temp = data[i];
			
			inner = {
				receiving : temp.fullDetails.receiving,
				receiving_index : temp.fullDetails.receiving_index,
				sent_first : temp.fullDetails.sent_first,
				sent_first_index : temp.fullDetails.sent_first_index,
				total_clicks : temp.fullDetails.total_clicks,
				unique_clicks : temp.fullDetails.unique_clicks,
				date_started : temp.fullDetails.date_started,
				banking_partner_id : temp.fullDetails.banking_partner_id, 
				first_name : temp.fullDetails.first_name,
				last_name : temp.fullDetails.last_name,
				banking_partner_name : temp.fullDetails.banking_partner_name,
				third_party_url_id : temp.fullDetails.third_party_url_id,
				third_party_url : temp.fullDetails.third_party_url,
				click_value_type : temp.fullDetails.click_value_type,
				number_of_rounds : temp.fullDetails.number_of_rounds,
				clicks_owed : temp.fullDetails.clicks_owed,
				status : temp.fullDetails.status,
				completed : temp.fullDetails.completed
			}
				
			returnValue.addItem(inner);
			
		}
	}
	
	return returnValue;

}

/**test*/
public function getTempArray(data:ArrayCollection, screen:String):ArrayCollection{
	
	var returnValue:ArrayCollection = new ArrayCollection();
	var temp:Object = new Object();
	var inner:Object;
	
	if(screen == 'home'){
	
		for(var k:String in data){
			temp = data[k];
			inner = new Object();
			
			inner.fullDetails = {
				receiving : temp.receiving,
				banking_partner_id : temp.banking_partner_id,
				banking_partner_name : temp.banking_partner_name,
				clicks_owed : temp.clicks_owed,
				total_clicks : temp.total_clicks,
				is_total : temp.is_total
			};
			
			returnValue.addItem(inner);
		}
	}else{
		for(var i:String in data){
			
			inner = new Object();
			
			temp = data[i];
			
			inner.fullDetails = {
				receiving : temp.receiving,
				receiving_index : temp.receiving_index,
				sent_first : temp.sent_first,
				sent_first_index : temp.sent_first_index,
				total_clicks : temp.total_clicks,
				unique_clicks : temp.unique_clicks,
				date_started : temp.date_started,
				banking_partner_id : temp.banking_partner_id, 
				first_name : temp.first_name,
				last_name : temp.last_name,
				banking_partner_name : temp.banking_partner_name,
				third_party_url_id : temp.third_party_url_id,
				third_party_url : temp.third_party_url,
				click_value_type : temp.click_value_type,
				number_of_rounds : temp.number_of_rounds,
				clicks_owed : temp.clicks_owed,
				status : temp.status,
				completed : temp.completed
			}
			
			returnValue.addItem(inner);
			
		}
	}
	
	return returnValue;
	
}

public function setSortFields(data:String, screen:String):Object{
	var returnValue:Object = new Object();
	switch(data)
	{
		case 'ID':
		{
			returnValue = {
				fieldName : 'banking_partner_id',
				isNumeric : true
			};
			break;
		}
			
		case 'Banking Partner':
		{
			returnValue = {
				fieldName : 'banking_partner_name',
				isNumeric : false
			};
			break;
		}
			
		case 'Receiving':
		{
			returnValue = {
				fieldName : (screen != 'partner management') ? 'clicks_owed' : 'receiving',
				isNumeric : (screen != 'partner management') ? true : false
			};
			break;
		}
			
		case 'Sending':
		{
			returnValue = {
				fieldName : 'clicks_owed',
				isNumeric : true
			};
			break;
		}
			
		case 'Banking Partner':
		{
			returnValue = {
				fieldName : 'banking_partner_name',
				isNumeric : false
			};
			break;
		}
			
		case 'Sent First':
		{
			returnValue = {
				fieldName : 'sent_first',
				isNumeric : false
			};
			break;
		}
			
		case 'Number of Rounds':
		{
			returnValue = {
				fieldName : 'number_of_rounds',
				isNumeric : true
			};
			break;
		}
		
		case 'Clicks Owed':
		{
			returnValue = {
				fieldName : 'clicks_owed',
				isNumeric : true
			}
		}
			
		default:
		{
			break;
		}
	}
	
	return returnValue;
}

public function getCurrentDate():String
{               
	var currentDateTime:Date = new Date();
	var currentDF:DateFormatter = new DateFormatter();
	//currentDF.formatString = "MM/DD/YY LL:NN:SS A"
	currentDF.formatString = "YYYY-MM-DD LL:NN:SS"
	var DateTimeString:String = currentDF.format(currentDateTime);
	return DateTimeString;
}
