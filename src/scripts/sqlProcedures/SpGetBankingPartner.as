package scripts.sqlProcedures
{
	
	import controller.classes.hash.MD5;
	
	import flash.data.SQLResult;
	import flash.data.SQLStatement;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import flash.events.SQLErrorEvent;
	import flash.events.SQLEvent;
	import flash.filesystem.File;
	import flash.utils.ByteArray;
	
	import mx.formatters.DateFormatter;
	import mx.utils.ObjectUtil;
	
	[Event(name="getResult", type="flash.events.Event")]
	[Event(name="getAuthenticationResult", type="flash.events.Event")]
	[Event(name="getBankingPartnersListResult", type="flash.events.Event")]
	[Event(name="getTopReceivingBankingPartnersResult", type="flash.events.Event")]
	[Event(name="getTopSendingBankingPartnersResult", type="flash.events.Event")]
	[Event(name="getReceivingBankingPartnersResult", type="flash.events.Event")]
	[Event(name="getSendingBankingPartnersResult", type="flash.events.Event")]
	[Event(name="getReceivingChartDataResult", type="flash.events.Event")]
	[Event(name="getSendingChartDataResult", type="flash.events.Event")]
	
	public class SpGetBankingPartner extends EventDispatcher
	{
		include "../../scripts/GlobalScript.as";
		include "../../scripts/formatters/GlobalFormatter.as";
		
		public var selectStmt:SQLStatement;
		private var insertStmt:SQLStatement;
		private var currentDateTime:Date = new Date();
		
		public var result:SQLResult;
		private var sqlData:Object;
		private var receiving:int;
		private var resultNames:SQLResult;
		public var bankingPartnersList:ArrayCollection;
		private var i:int = 0;
		private var inner:Object;

		public var chartData:ArrayCollection;
		public var receivingChartData:ArrayCollection;
		public var sendingChartData:ArrayCollection;
		
		public function SpGetBankingPartner(target:IEventDispatcher=null)
		{
			initializeDatabase();
		}
				
		public function getTopBankingPartners(data:Object):void
		{
			initializeDatabase();
			
			receiving = int(data.receiving);
			selectStmt = new SQLStatement();
			selectStmt.sqlConnection = conn;
			
			var sql:String = "SELECT banking_partners.id as banking_partner_id," +
				" banking_partners.user_profile_id," +
				" banking_partners.receiving," +
				" banking_partners.clicks_owed," +
				" banking_partners.unique_clicks," +
				" banking_partners.third_party_url_id," +
				" banking_partners.third_party_url," +
				" user_profiles.first_name," +
				" user_profiles.last_name" +
				" FROM banking_partners" +
				" JOIN user_profiles ON banking_partners.user_profile_id = user_profiles.id" +
				" WHERE banking_partners.deleted = 0" +
				" AND banking_partners.receiving != "+receiving+""+
				" AND user_profiles.id = banking_partners.user_profile_id" +
				" ORDER BY banking_partners.clicks_owed DESC;";
			
			selectStmt.text = sql;
			
			selectStmt.addEventListener(SQLEvent.RESULT, getTopReceivingBankingPartnersResult);
			selectStmt.addEventListener(SQLErrorEvent.ERROR, getTopReceivingBankingPartnersError);
			
			selectStmt.execute();
		}
		
		protected function getTopReceivingBankingPartnersResult(event:SQLEvent):void
		{
			result = new SQLResult();
			result = selectStmt.getResult();
			conn.close();
			if(receiving == 1){
				dispatchEvent(new Event('getTopReceivingBankingPartnersResult'));
			}else if(receiving == 2){
				dispatchEvent(new Event('getTopSendingBankingPartnersResult'));
			}
			
		}
		
		protected function getTopReceivingBankingPartnersError(event:SQLErrorEvent):void
		{
			// TODO Auto-generated method stub
			
		}
		
		
		public function getBankingPartnersList():void
		{
			selectStmt = new SQLStatement();
			selectStmt.sqlConnection = conn;
			var sql:String = "SELECT banking_partners.id as banking_partner_id,"+
								" banking_partners.user_profile_id,"+
								" banking_partners.sent_first,"+
								" banking_partners.receiving,"+
								" banking_partners.number_of_rounds,"+
								" banking_partners.clicks_owed,"+
								" banking_partners.third_party_url_id,"+
								" banking_partners.third_party_url,"+
								" banking_partners.total_clicks,"+
								" banking_partners.unique_clicks,"+
								" banking_partners.subscribers_received,"+
								" banking_partners.clicks_received,"+
								" banking_partners.status,"+
								" banking_partners.date_started,"+
								" user_profiles.first_name,"+
								" user_profiles.last_name"+
																					    
							" FROM banking_partners"+
							" JOIN user_profiles ON banking_partners.user_profile_id = user_profiles.id"+
																
							" WHERE banking_partners.deleted = 0;";
			selectStmt.text = sql;
			
			selectStmt.addEventListener(SQLEvent.RESULT, getBankingPartnersListResult);
			selectStmt.addEventListener(SQLErrorEvent.ERROR, getBankingPartnersListError);
			
			selectStmt.execute();
		}
		
		protected function getBankingPartnersListResult(event:SQLEvent):void
		{
			result = new SQLResult();
			
			result = selectStmt.getResult();
			
			if(result.data != null){
				
				if(result.data.length > 0){
					getThirdPartyURLName();
				}
				
			}
		}
		
		private function getThirdPartyURLName():void
		{
			selectStmt = new SQLStatement();
			selectStmt.sqlConnection = conn;
			var sql:String = "SELECT banking_partners.id as banking_partner_id,"+
				" banking_partners.third_party_url_id,"+
				" third_party_urls.name"+
				
				" FROM banking_partners"+
				" JOIN user_profiles ON banking_partners.user_profile_id = user_profiles.id"+
				" JOIN third_party_urls ON banking_partners.third_party_url_id = third_party_urls.id"+
				
				" WHERE banking_partners.deleted = 0;";
			selectStmt.text = sql;
			
			selectStmt.addEventListener(SQLEvent.RESULT, getThirdPartyURLNameResult);
			selectStmt.addEventListener(SQLErrorEvent.ERROR, getThirdPartyURLNameError);
			
			selectStmt.execute();
		}
		
		protected function getThirdPartyURLNameResult(event:SQLEvent):void
		{
			resultNames = new SQLResult();
			resultNames = selectStmt.getResult();
			
			formatBankingPartners();
		}
		
		private function formatBankingPartners():void
		{
			bankingPartnersList = new ArrayCollection();
			
			var innerResultData:Object = new Object();
			var inner:Object = new Object();
			var temp:Object = new Object();
						
			for(var i:String in result.data){
				innerResultData = new Object();
				innerResultData = result.data[i];
				inner = new Object();
				
				if(resultNames.data != null){
					for(var j:String in resultNames.data){
						temp  = new Object();
						temp = resultNames.data[j];
						
						if(innerResultData.banking_partner_id == resultNames.data[j].banking_partner_id){
							
							inner = new Object();
							inner = {
								banking_partner_id : innerResultData.banking_partner_id,
								user_profile_id : innerResultData.user_profile_id,
								sent_first : innerResultData.sent_first,
								receiving : innerResultData.receiving,
								number_of_rounds : innerResultData.number_of_rounds,
								clicks_owed : innerResultData.clicks_owed,
								third_party_url_id : innerResultData.third_party_url_id,
								third_party_url : innerResultData.third_party_url,
								third_party_url_name : temp.name,
								total_clicks : innerResultData.total_clicks,
								unique_clicks : innerResultData.unique_clicks,
								subscribers_received : innerResultData.subscribers_received,
								clicks_received : innerResultData.clicks_received,
								status : innerResultData.status,
								date_started : innerResultData.date_started,
								first_name : innerResultData.first_name,
								last_name : innerResultData.last_name
							};
							
							bankingPartnersList.addItem(inner);
							
						}
					}
				}
				inner = new Object();
				inner = {
					banking_partner_id : innerResultData.banking_partner_id,
						user_profile_id : innerResultData.user_profile_id,
						sent_first : innerResultData.sent_first,
						receiving : innerResultData.receiving,
						number_of_rounds : innerResultData.number_of_rounds,
						clicks_owed : innerResultData.clicks_owed,
						third_party_url_id : innerResultData.third_party_url_id,
						third_party_url : innerResultData.third_party_url,
						third_party_url_name : '',
						total_clicks : innerResultData.total_clicks,
						unique_clicks : innerResultData.unique_clicks,
						subscribers_received : innerResultData.subscribers_received,
						clicks_received : innerResultData.clicks_received,
						status : innerResultData.status,
						date_started : innerResultData.date_started,
						first_name : innerResultData.first_name,
						last_name : innerResultData.last_name
				};
								
				if(innerResultData.third_party_url_id == 0){
					bankingPartnersList.addItem(inner);
				}
				
			}
			
			dispatchEvent(new Event('getBankingPartnersListResult'));
		}
		
		protected function getThirdPartyURLNameError(event:SQLErrorEvent):void
		{
			
		}
				
		protected function getBankingPartnersListError(event:SQLErrorEvent):void
		{
			trace(event.text);
			trace(event.error);
		}
		
		public function getBankingPartners(data:Object):void
		{
			initializeDatabase();
			
			receiving = int(data.receiving);
			selectStmt = new SQLStatement();
			selectStmt.sqlConnection = conn;
			
			var sql:String = "SELECT banking_partners.id as banking_partner_id," +
					" banking_partners.user_profile_id," +
					" banking_partners.third_party_url_id," +
					" banking_partners.clicks_owed," +
					" banking_partners.unique_clicks," +
					" user_profiles.first_name," +
					" user_profiles.last_name" +
				" FROM banking_partners" +
				" JOIN user_profiles ON banking_partners.user_profile_id = user_profiles.id"+
				" WHERE banking_partners.deleted = 0" +
				" AND banking_partners.receiving != "+receiving+""+
				" ORDER BY banking_partners.clicks_owed DESC;";
			
			selectStmt.text = sql;
			
			selectStmt.addEventListener(SQLEvent.RESULT, getBankingPartnersResult);
			selectStmt.addEventListener(SQLErrorEvent.ERROR, getBankingPartnersError);
			
			selectStmt.execute();
		}
		
		protected function getBankingPartnersResult(event:SQLEvent):void
		{
			result = new SQLResult();
			result = selectStmt.getResult();
			conn.close();
			
			if(receiving == 1){
				dispatchEvent(new Event('getReceivingBankingPartnersResult'));
			}else if(receiving == 2){
				dispatchEvent(new Event('getSendingBankingPartnersResult'));
			}
		}
		
		protected function getBankingPartnersError(event:SQLErrorEvent):void
		{
			
		}
		
		public function getChartData(data:Object):void
		{
			initializeDatabase();
			
			selectStmt = new SQLStatement();
			selectStmt.sqlConnection = conn;
			
			receiving = int(data.receiving);
			
			receivingChartData = new ArrayCollection();
			sendingChartData = new ArrayCollection();
			
			i = 1;
			
			for(i = 1; i<=12; i++){
			
				var sql:String = "SELECT strftime('%m', date_created) as month," +
					" SUM(total_clicks) as total_clicks" +
					" FROM banking_partners" +
					" WHERE strftime('%Y', date_created)='2014'" +
					" AND month = '"+i+"'" +
					" AND receiving != "+receiving+""+
					" GROUP BY month";
			
				selectStmt.text = sql;
			
				selectStmt.addEventListener(SQLEvent.RESULT, getChartDataResult);
				selectStmt.addEventListener(SQLErrorEvent.ERROR, getChartDataError);
				
				selectStmt.execute();
			}
			
		}
		
		protected function getChartDataResult(event:SQLEvent):void
		{
			result = new SQLResult();
			result = selectStmt.getResult();
			
			if(i == 12){
				formatChartData((result.data != null) ? result.data : new Array);
			}
			
			/*if(result.data != null){
				inner = new Object();
				
				inner = result.data[0];
				
				if(receiving == 1){
					receivingChartData.addItem(inner);
				}else if(receiving == 2){
					sendingChartData.addItem(inner);
				}
				
				if(i == 12){
					if(receiving == 1){
						dispatchEvent(new Event('getReceivingChartDataResult'));
					}else if(receiving == 2){
						dispatchEvent(new Event('getSendingChartDataResult'));
					}
				}
			}*/
			
		}
		
		private function formatChartData(data:Array):void
		{
			var innerMonths:Object;
			
			var months:ArrayCollection = new ArrayCollection([
				{month: "January", total_clicks : 0},
				{month: "February", total_clicks : 0},
				{month: "March", total_clicks : 0},
				{month: "April", total_clicks : 0},
				{month: "May", total_clicks : 0},
				{month: "June", total_clicks : 0},
				{month: "July", total_clicks : 0},
				{month: "August", total_clicks : 0},
				{month: "September", total_clicks : 0},
				{month: "October", total_clicks : 0},
				{month: "November", total_clicks : 0},
				{month: "December", total_clicks : 0}
			]);
			
			for(var x:String in months){
				
				innerMonths = new Object();
				innerMonths = months[x];
				
				for(var y:String in data){
					//trace(months.getItemAt((data[y].month-1)).month);
					months.setItemAt({month: months.getItemAt((data[y].month-1)).month, total_clicks : data[y].total_clicks}, (data[y].month-1));
				}
				
			}
			
			trace(ObjectUtil.toString(months));
			
			if(receiving == 1){
				receivingChartData = months;
				dispatchEvent(new Event('getReceivingChartDataResult'));
			}else if(receiving == 2){
				sendingChartData = months;
				dispatchEvent(new Event('getSendingChartDataResult'));
			}
						
		}
		
		protected function getChartDataError(event:SQLErrorEvent):void
		{
			// TODO Auto-generated method stub
			
		}
		
	}
}