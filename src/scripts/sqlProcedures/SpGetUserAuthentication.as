package scripts.sqlProcedures
{
	import controller.classes.hash.MD5;
	
	import flash.data.SQLResult;
	import flash.data.SQLStatement;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import flash.events.SQLErrorEvent;
	import flash.events.SQLEvent;
	
	import mx.utils.ObjectUtil;
	
	[Event(name="getResult", type="flash.events.Event")]
	[Event(name="getAuthenticationResult", type="flash.events.Event")]
	[Event(name="getAuthenticationResultError", type="flash.events.Event")]
	
	public class SpGetUserAuthentication extends EventDispatcher
	{
		
		include "../../scripts/GlobalScript.as";
		include "../../scripts/formatters/GlobalFormatter.as";
		
		private var selectStmt:SQLStatement;
		public var result:SQLResult;
		private var insertStmt:SQLStatement;
		private var sqlData:Object;
		
		public function SpGetUserAuthentication(target:IEventDispatcher=null)
		{
			initializeDatabase();
		}
		
		public function getUserCount():void
		{	
			selectStmt = new SQLStatement();
			selectStmt.sqlConnection = conn;
			
			var sql:String = "SELECT count(users.id) as user_count FROM users where users.deleted = 0";
			selectStmt.text = sql;
			
			selectStmt.addEventListener(SQLEvent.RESULT, selectResult);
			selectStmt.addEventListener(SQLErrorEvent.ERROR, selectError);
			
			selectStmt.execute();
		}
		
		protected function selectResult(event:SQLEvent):void
		{
			result = new SQLResult();
			
			result = selectStmt.getResult();
			
			if(result.data != null){
				if(result.complete){
					selectStmt.removeEventListener(SQLEvent.RESULT, selectResult);
					dispatchEvent(new Event('getResult'));
				}
			}
		}
		
		protected function selectError(event:SQLErrorEvent):void
		{
			trace(event.text);
			trace(event.error);
		}		
						
		public function authenticateUser(data:Object):void
		{
			sqlData = new Object();
			
			sqlData = data;
			
			selectStmt = new SQLStatement();
			selectStmt.sqlConnection = conn;
			var sql:String = "SELECT users.id "+
				"FROM users "+
				"WHERE users.user_name = '"+data.user_name+"' "+
				"AND users.password = '"+MD5.hash(String(data.password))+"';";
			
			selectStmt.text = sql;
			
			selectStmt.addEventListener(SQLEvent.RESULT, getUserIDResult);
			selectStmt.addEventListener(SQLErrorEvent.ERROR, getUserIDError);
			
			selectStmt.execute();
			
		}
		
		protected function getUserIDError(event:SQLErrorEvent):void
		{
			trace(event.text);
			trace(event.error);
		}
		
		protected function getUserIDResult(event:SQLEvent):void
		{
			result = new SQLResult();
			
			result = selectStmt.getResult();
						
			if(result.data != null){
				insertStmt = new SQLStatement();
				insertStmt.sqlConnection = conn;
				var sql1:String = "INSERT INTO logs (action, identifier_id, identifier_table, deleted, created_by, modified_by, date_created, date_modified) ";
				sql1 += "VALUES ('User Login', "+String(result.data[0].id)+", 'users', 0, "+int(session.user_id)+", "+int(session.user_id)+", '"+getCurrentDate()+"','"+getCurrentDate()+"');";
				
				insertStmt.text = sql1;
				
				insertStmt.addEventListener(SQLEvent.RESULT, insertResult);
				insertStmt.addEventListener(SQLErrorEvent.ERROR, insertError);
				
				insertStmt.execute();
				
				selectStmt = new SQLStatement();
				selectStmt.sqlConnection = conn;
				var sql2:String = "SELECT users.id as user_id,"+
					" users.user_name,"+
					" users.license,"+
					" logs.date_created as last_logged_in"+
					" FROM users"+
					" join logs on users.id = logs.identifier_id"+
					" WHERE users.user_name = '"+sqlData.user_name+"'"+
					" AND users.password = '"+MD5.hash(String(sqlData.password))+"'"+
					" AND logs.identifier_id = "+String(result.data[0].id)+
					" AND logs.action = 'User Login'"+
					" AND logs.identifier_table = 'users'"+
					" ORDER BY logs.id DESC limit 1;";
				
				selectStmt.text = sql2;
				
				selectStmt.addEventListener(SQLEvent.RESULT, authenticateUserResult);
				selectStmt.addEventListener(SQLErrorEvent.ERROR, authenticateUserError);
				
				selectStmt.execute();
			}else{
				
				dispatchEvent(new Event('getAuthenticationResultError'));
				
			}
		}
		
		protected function insertError(event:SQLErrorEvent):void
		{
			trace(event.text);
			trace(event.error);
		}
		
		protected function insertResult(event:SQLEvent):void
		{
			// TODO Auto-generated method stub
			
		}
		
		protected function authenticateUserError(event:SQLErrorEvent):void
		{
			trace(event.text);
			trace(event.error);		
		}
		
		protected function authenticateUserResult(event:SQLEvent):void
		{
			result = new SQLResult();
			result = selectStmt.getResult();
			
			dispatchEvent(new Event('getAuthenticationResult'));
		}

		
		
		
		
	}
}