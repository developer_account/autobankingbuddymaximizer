package scripts.sqlProcedures
{
	import flash.data.SQLStatement;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import flash.events.SQLErrorEvent;
	import flash.events.SQLEvent;
	import flash.data.SQLResult;
	import flash.events.Event;
	
	[Event(name="getThirdPartyUrlsResult", type="flash.events.Event")]
	
	public class SpGetThirdPartyUrl extends EventDispatcher
	{
		
		include "../../scripts/GlobalScript.as";
		private var selectStmt:SQLStatement;
		public var result:SQLResult;
		
		public function SpGetThirdPartyUrl(target:IEventDispatcher=null)
		{
			initializeDatabase();
		}
		
		public function getThirdPartyUrls():void
		{
			selectStmt = new SQLStatement();
			selectStmt.sqlConnection = conn;
			var sql:String = "SELECT third_party_urls.id," +
							" third_party_urls.name" +
							" FROM third_party_urls" +
							" WHERE third_party_urls.deleted = 0;";
			
			selectStmt.text = sql;
			
			selectStmt.addEventListener(SQLEvent.RESULT, getThirdPartyUrlsResult);
			selectStmt.addEventListener(SQLErrorEvent.ERROR, getThirdPartyUrlsError);
			
			selectStmt.execute();
		}
		
		protected function getThirdPartyUrlsResult(event:SQLEvent):void
		{
			result = new SQLResult();
			
			result = selectStmt.getResult();
			
			//conn.close();
			dispatchEvent(new Event('getThirdPartyUrlsResult'));
		}
		
		protected function getThirdPartyUrlsError(event:SQLErrorEvent):void
		{
			trace(event.text);
			trace(event.error);
		}
		
	}
}