package scripts.sqlProcedures
{
	import controller.classes.hash.MD5;
	
	import flash.data.SQLResult;
	import flash.data.SQLStatement;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import flash.events.SQLErrorEvent;
	import flash.events.SQLEvent;
	
	import mx.utils.ObjectUtil;
	
	[Event(name="registerUserResult", type="flash.events.Event")]
	
	public class SpUpdateUserAuthentication extends EventDispatcher
	{
		include "../../scripts/GlobalScript.as";
		include "../../scripts/formatters/GlobalFormatter.as";
		
		private var selectStmt:SQLStatement;
		private var userData:Object;
		public var result:SQLResult;
		private var insertStmt:SQLStatement;
		private var sql:String;
		
		public function SpUpdateUserAuthentication(target:IEventDispatcher=null)
		{
			initializeDatabase();
		}
		
		public function searchDuplicates(data:Object):void
		{
			userData = new Object();
			userData = data;
			
			selectStmt = new SQLStatement();
			selectStmt.sqlConnection = conn;
			
			sql = "";
			
			sql = "SELECT count(id) as count FROM users" +
				" WHERE user_name = '"+userData.user_name+"'"+
				" AND password = '"+MD5.hash(userData.password)+"'"+
				" AND license = '"+userData.license+"';";
			
			selectStmt.text = sql;
			
			selectStmt.addEventListener(SQLEvent.RESULT, searchForDuplicateUsersResult);
			selectStmt.addEventListener(SQLErrorEvent.ERROR, searchForDuplicateUsersError);
			
			selectStmt.execute();
			
		}
		
		protected function searchForDuplicateUsersResult(event:SQLEvent):void
		{
			result = new SQLResult();
			result  = selectStmt.getResult();
			
			if(int(result.data[0].count) == 0){
				registerUser();
			}
			
		}
		
		protected function searchForDuplicateUsersError(event:SQLErrorEvent):void
		{
			trace(event.text);
			trace(event.error);
		}
		
		private function registerUser():void
		{
			insertStmt = new SQLStatement();
			insertStmt.sqlConnection = conn;
			
			sql = "INSERT INTO users" +
					" (user_type_id," +
					" user_name," +
					" password," +
					" license," +
					" verified_license," +
					" deleted," +
					" created_by," +
					" modified_by," +
					" date_created," +
					" date_modified)" +
				" VALUES" +
					" (1," +
					" '"+userData.user_name+"'," +
					" '"+MD5.hash(userData.password)+"'," +
					" '"+userData.license+"'," +
					" 1," +
					" 0," +
					" 0," +
					" 0," +
					" '"+getCurrentDate()+"'," +
					" '"+getCurrentDate()+"');";
			
			insertStmt.text = sql;
			
			insertStmt.addEventListener(SQLEvent.RESULT, registerUserResult);
			insertStmt.addEventListener(SQLErrorEvent.ERROR, registerUserError);
			
			insertStmt.execute();
			
			sql = '';
			
			sql = 'SELECT last_insert_rowid() as success';
			
			selectStmt = new SQLStatement();
			selectStmt.sqlConnection = conn;
			
			selectStmt.text = sql;
			
			selectStmt.addEventListener(SQLEvent.RESULT, getLastInsertedUserIdResult);
			selectStmt.addEventListener(SQLErrorEvent.ERROR, getLastInsertedUserError);
			selectStmt.execute();
			
		}
		
		
		protected function registerUserResult(event:SQLEvent):void
		{
			
		}
		
		protected function registerUserError(event:SQLErrorEvent):void
		{
			// TODO Auto-generated method stub
			
		}
		
		protected function getLastInsertedUserIdResult(event:SQLEvent):void
		{
			result = new SQLResult();
			
			result = selectStmt.getResult();
			
			if(result != null){
				dispatchEvent(new Event('registerUserResult'));
			}
		}
		
		protected function getLastInsertedUserError(event:SQLErrorEvent):void
		{
			trace(event.text);
			trace(event.error);
		}
		
		
	}
}