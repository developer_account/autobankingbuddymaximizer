package scripts.sqlProcedures
{
	import flash.data.SQLCollationType;
	import flash.data.SQLResult;
	import flash.data.SQLStatement;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import flash.events.SQLErrorEvent;
	import flash.events.SQLEvent;
	
	import mx.utils.ObjectUtil;
	
	[Event(name="addBankingPartnerResult", type="flash.events.Event")]
	[Event(name="editBankingPartnerResult", type="flash.events.Event")]
	[Event(name="deleteBankingPartnerResult", type="flash.events.Event")]
	
	public class SpUpdateBankingPartner extends EventDispatcher
	{
		include "../../scripts/GlobalScript.as";
		include "../../scripts/formatters/GlobalFormatter.as";
		
		private var selectStmt:SQLStatement;
		private var bankingPartnerData:Object;
		private var insertStmt:SQLStatement;
		private var updateStmt:SQLStatement;
		public var result:SQLResult;
		public var success:Object;
		private var response:int = 0;
		private var loops:int = 0;
		
		public function SpUpdateBankingPartner(target:IEventDispatcher=null)
		{
			initializeDatabase();
			initializeSession();
		}
		
		public function updateBankingPartnerThirdPartyValues(data:Object):void
		{
			initializeDatabase();
			selectStmt = new SQLStatement();
			selectStmt.sqlConnection = conn;
			var sql:String = "UPDATE banking_partners"+
				" SET"+
				" total_clicks = "+data.total_clicks+","+
				" unique_clicks = "+data.unique_clicks+","+
				" modified_by = 1,"+
				" date_modified = '"+getCurrentDate()+"'"+
				" WHERE id = '"+data.banking_partner_id+"';";
			
			selectStmt.text = sql;
			
			selectStmt.addEventListener(SQLEvent.RESULT, updateBankingPartnerThirdPartyValuesResult);
			selectStmt.addEventListener(SQLErrorEvent.ERROR, updateBankingPartnerThirdPartyValuesError);
			
			selectStmt.execute();
		}
		
		protected function updateBankingPartnerThirdPartyValuesResult(event:SQLEvent):void
		{
			
		}
		
		protected function updateBankingPartnerThirdPartyValuesError(event:SQLErrorEvent):void
		{
			trace(event.text);
			trace(event.error);
		}
		
		public function addBankingPartner(data:Object):void
		{
			initializeDatabase();
			
			bankingPartnerData = new Object();
			bankingPartnerData = data;
						
			insertStmt = new SQLStatement();
			insertStmt.sqlConnection = conn;
						
			var sql1:String = "INSERT INTO user_profiles" +
				" (user_id," +
				" first_name," +
				" last_name," +
				" deleted," +
				" created_by," +
				" modified_by," +
				" date_created," +
				" date_modified)" +
				" VALUES"+
				" (0," +
				" '"+bankingPartnerData.first_name+"'," +
				" '"+bankingPartnerData.last_name+"'," +
				" 0," +
				" "+String(session.user_id)+"," +
				" "+String(session.user_id)+"," +
				" '"+getCurrentDate()+"'," +
				" '"+getCurrentDate()+"');";
			
			insertStmt.text = sql1;
			
			insertStmt.addEventListener(SQLEvent.RESULT, insertUserProfileResult);
			insertStmt.addEventListener(SQLErrorEvent.ERROR, insertUserProfileError);
			
			insertStmt.execute();
			
			sql1 = '';
			
			sql1 = 'SELECT last_insert_rowid() as user_profile_id';
			
			selectStmt = new SQLStatement();
			selectStmt.sqlConnection = conn;
				
			selectStmt.text = sql1;
			
			selectStmt.addEventListener(SQLEvent.RESULT, getUserProfileIdResult);
			selectStmt.addEventListener(SQLErrorEvent.ERROR, getUserProfileIdError);
			selectStmt.execute();
						
			data = null;
		}
		
		protected function getUserProfileIdResult(event:SQLEvent):void
		{
			
			result = new SQLResult();
			
			result = selectStmt.getResult();
			
			if(result != null){
				if(int(bankingPartnerData.mode) == 1){
					insertBankingPartner(result);
				}else if(int(bankingPartnerData.mode) == 4){
					conn.close();
					updateUserProfiles(result);
				}
			}
		}
		
		private function insertBankingPartner(userProfileId:SQLResult):void
		{	
			initializeDatabase();
			
			var sql:String = "";
			
			insertStmt = new SQLStatement();
			insertStmt.sqlConnection = conn;
			
			sql = "INSERT INTO banking_partners" +
				" (user_profile_id," +
				" sent_first," +
				" receiving," +
				" number_of_rounds," +
				" clicks_owed," +
				" third_party_url_id," +
				" third_party_url," +
				" total_clicks," +
				" unique_clicks," +
				" status," +
				" date_started," +
				" deleted," +
				" created_by," +
				" modified_by," +
				" date_created," +
				" date_modified)" +
			" VALUES" +
				" ("+userProfileId.data[0].user_profile_id+"," +
				" "+bankingPartnerData.sent_first+"," +
				" "+bankingPartnerData.receiving+"," +
				" "+bankingPartnerData.number_of_rounds+"," +
				" "+bankingPartnerData.clicks_owed+"," +
				" "+bankingPartnerData.third_party_url_id+"," +
				" '"+bankingPartnerData.third_party_url+"'," +
				" "+bankingPartnerData.total_clicks+"," +
				" '"+bankingPartnerData.unique_clicks+"'," +
				" "+bankingPartnerData.status+"," +
				" '"+getCurrentDate()+"'," +
				" 0," +
				" "+session.user_id+"," +
				" "+session.user_id+"," +
				" '"+getCurrentDate()+"'," +
				" '"+getCurrentDate()+"');";
			
			insertStmt.text = sql;
			
			insertStmt.addEventListener(SQLEvent.RESULT, insertBankingPartnerResult);
			insertStmt.addEventListener(SQLErrorEvent.ERROR, insertBankingPartnerError);
			
			insertStmt.execute();
			
			sql = '';
			
			sql = 'SELECT last_insert_rowid() as success';
			
			selectStmt = new SQLStatement();
			selectStmt.sqlConnection = conn;
			
			selectStmt.text = sql;
			
			selectStmt.addEventListener(SQLEvent.RESULT, getBankingPartnerIdResult);
			selectStmt.addEventListener(SQLErrorEvent.ERROR, getBankingPartnerIdError);
			selectStmt.execute();
			
		}
		
		protected function insertBankingPartnerResult(event:SQLEvent):void
		{
			
		}
		
		protected function insertBankingPartnerError(event:SQLErrorEvent):void
		{
			// TODO Auto-generated method stub
			
		}		
		
		protected function getBankingPartnerIdResult(event:SQLEvent):void
		{
			result = new SQLResult();
			
			result = selectStmt.getResult();
			
			if(result != null){
				dispatchEvent(new Event('addBankingPartnerResult'));
			}
		}		
		
		protected function getBankingPartnerIdError(event:SQLErrorEvent):void
		{
			trace(event.text);
			trace(event.error);
		}
		
		protected function getUserProfileIdError(event:SQLErrorEvent):void
		{
			trace(event.text);
			trace(event.error);
		}
		
		protected function insertUserProfileError(event:SQLErrorEvent):void
		{
			trace(event.text);
			trace(event.error);
		}
		
		protected function insertUserProfileResult(event:SQLEvent):void
		{
			
		}
		
		public function editBankingPartner(data:Object):void
		{
			initializeDatabase();
			bankingPartnerData = new Object();
			bankingPartnerData = data;
			
			updateStmt = new SQLStatement();
			updateStmt.sqlConnection = conn;
						
			var sql:String = "UPDATE banking_partners" +
				" SET sent_first = "+bankingPartnerData.sent_first+"," +
				" receiving = "+bankingPartnerData.receiving+"," +
				" number_of_rounds = "+bankingPartnerData.number_of_rounds+"," +
				" clicks_owed = "+bankingPartnerData.clicks_owed+"," +
				" third_party_url_id = "+bankingPartnerData.third_party_url_id+"," +
				" third_party_url = '"+bankingPartnerData.third_party_url+"'," +
				" total_clicks = "+bankingPartnerData.total_clicks+"," +
				" unique_clicks = '"+bankingPartnerData.unique_clicks+"'," +
				" status = "+bankingPartnerData.status+"," +
				" modified_by = "+session.user_id+"," +
				" date_modified = '"+getCurrentDate()+"'" +
				" WHERE id = "+bankingPartnerData.banking_partner_ids+";";
			
			updateStmt.text = sql;
			
			updateStmt.addEventListener(SQLEvent.RESULT, updateBankingPartnerResult);
			updateStmt.addEventListener(SQLErrorEvent.ERROR, updateBankingPartnerError);
			
			updateStmt.execute();
			
			sql = '';
			
			sql = "SELECT banking_partners.user_profile_id" +
				" FROM banking_partners" +
				" WHERE banking_partners.id = '"+bankingPartnerData.banking_partner_ids+"';";
			
			selectStmt = new SQLStatement();
			selectStmt.sqlConnection = conn;
			
			selectStmt.text = sql;
			
			selectStmt.addEventListener(SQLEvent.RESULT, getUserProfileIdResult);
			selectStmt.addEventListener(SQLErrorEvent.ERROR, getUserProfileIdError);
			selectStmt.execute();
			
			data = null;
		}
		
		protected function updateBankingPartnerResult(event:SQLEvent):void
		{
			// TODO Auto-generated method stub
			
		}
		
		protected function updateBankingPartnerError(event:SQLErrorEvent):void
		{
			trace(event.text);
			trace(event.error);
		}
		
		private function updateUserProfiles(userProfileId:SQLResult):void
		{			
			initializeDatabase();
			updateStmt = new SQLStatement();
			updateStmt.sqlConnection = conn;
			
			var sql:String = "UPDATE user_profiles" +
				" SET first_name = '"+bankingPartnerData.first_name+"'," +
				" last_name = '"+bankingPartnerData.last_name+"'," +
				" modified_by = '"+session.user_id+"'," +
				" date_modified = '"+getCurrentDate()+"'" +
				" WHERE id = "+userProfileId.data[0].user_profile_id+";";
			
			updateStmt.text = sql;
			
			updateStmt.addEventListener(SQLEvent.RESULT, updateUserProfileResult);
			updateStmt.addEventListener(SQLErrorEvent.ERROR, updateUserProfileError);
			
			updateStmt.execute();
			
			sql = '';
			
			sql = "SELECT banking_partners.user_profile_id" +
				" FROM banking_partners" +
				" WHERE banking_partners.id = '"+bankingPartnerData.banking_partner_ids+"';";
			
			selectStmt = new SQLStatement();
			selectStmt.sqlConnection = conn;
			
			selectStmt.text = sql;
			
			selectStmt.addEventListener(SQLEvent.RESULT, getSuccessResult);
			selectStmt.addEventListener(SQLErrorEvent.ERROR, getSuccessError);
			selectStmt.execute();
			
			
			
		}
		
		protected function getSuccessResult(event:SQLEvent):void
		{
			success = new Object();
			
			success = conn.totalChanges;
			
			conn.close();
			
			dispatchEvent(new Event('editBankingPartnerResult'));
			
		}
		
		protected function getSuccessError(event:SQLErrorEvent):void
		{
			// TODO Auto-generated method stub
			
		}
		
		
		protected function updateUserProfileResult(event:SQLEvent):void
		{
			
		}
		
		protected function updateUserProfileError(event:SQLErrorEvent):void
		{
			// TODO Auto-generated method stub
			
		}
		
		public function deleteBankingPartners(data:Object):void
		{
			var tempIds:Array = new Array();
			var inner:Object = new Object();
			
			tempIds = String(data.banking_partner_ids).split(',');
			loops = int(data.loops);
			
			initializeDatabase();
			updateStmt = new SQLStatement();
			updateStmt.sqlConnection = conn;
			/*trace(ObjectUtil.toString(tempIds));*/
						
			for(var i:int = 0; i<tempIds.length; i++){
				inner = new Object();
				
				inner = tempIds[i];
				
				var sql:String = "UPDATE banking_partners" +
					" SET deleted = 1," +
					" modified_by = '"+session.user_id+"'," +
					" date_modified = '"+getCurrentDate()+"'" +
					" WHERE id = '"+inner+"';";
				
				updateStmt.text = sql;
				
				updateStmt.addEventListener(SQLEvent.RESULT, deleteBankingPartnerResult);
				updateStmt.addEventListener(SQLErrorEvent.ERROR, deleteBankingPartnerError);
				
				updateStmt.execute();
				
			}
			
			data = null;
		}
		
		protected function deleteBankingPartnerResult(event:SQLEvent):void
		{
			response++;
			
			if(response == loops){
				response = 0;
				loops = 0;
				success = new Object();
				
				success = conn.totalChanges;
				
				conn.close();
				
				dispatchEvent(new Event('deleteBankingPartnerResult'));
			}
			
			
		}
		
		protected function deleteBankingPartnerError(event:SQLErrorEvent):void
		{
			// TODO Auto-generated method stub
			
		}
	}
	
	
}