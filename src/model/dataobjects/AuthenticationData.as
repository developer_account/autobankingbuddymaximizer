package model.dataobjects
{
	[RemoteClass(alias="AuthenticationData")]
	[Bindable]
	public class AuthenticationData
	{
		public var mode:int;
		public var user_id:String;
		public var user_name:String;
		public var password:String;
		public var count:int;
	}
}