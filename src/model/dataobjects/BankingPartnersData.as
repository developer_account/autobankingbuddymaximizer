package model.dataobjects
{
	[RemoteClass(alias="BankingPartnersData")]
	public class BankingPartnersData
	{
		public var mode:int;
		public var id:int;
		public var banking_partner_id:int;
		public var user_profile_id:int;
		public var sent_first:int;
		public var receiving:int;
		public var number_of_rounds:int;
		public var clicks_owed:int;
		public var third_party_url_id:int;
		public var subscribers_received:int;
		public var clicks_received:int;
		public var date_started:String;
		public var first_name:String;
		public var middle_name:String;
		public var last_name:String;
		public var success:int;
		
		/*public function BankingPartnersData()
		{
			public var mode:int;
			public var id:int;
			public var banking_partner_id:int;
			public var user_profile_id:int;
			public var sent_first:int;
			public var receiving:int;
			public var number_of_rounds:int;
			public var clicks_owed;
			public var subscribers_received;
			public var clicks_received;
			public var date_started;
			public var first_name;
			public var middle_name;
			public var last_name;
			public var success;
		}*/
	}
}