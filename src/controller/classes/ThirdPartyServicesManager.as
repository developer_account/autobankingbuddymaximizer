package controller.classes
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	
	import mx.collections.ArrayCollection;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.remoting.mxml.RemoteObject;
	import mx.utils.ObjectUtil;
	
	import scripts.events.ThirdPartyServicesEvents;
	import scripts.lib.DataServiceManager;
	import scripts.sqlProcedures.SpGetThirdPartyUrl;
	import scripts.sqlProcedures.SpUpdateBankingPartner;
	
	/**
	 * ThirdPartyServicesManager
	 * <br/>Handles ThirdPartyServices operations
	 * @type EventDispatcher
	 * @author Jerusalem Alvaira
	 */
	public class ThirdPartyServicesManager extends EventDispatcher
	{
		private var serviceManager:DataServiceManager;
		
		private var thirdPartyDataServiceManager:RemoteObject;
		private var clickMagickDataServiceManager:RemoteObject;
		private var qccDataServiceManager:RemoteObject;
		private var cbDataServiceManager:RemoteObject;
		
		private var requestCount:int = 0;
		private var responseCount:int = 0;
		
		private var cmRequestCount:int = 0;
		private var cmResponseCount:int = 0;
		private var clickMagicValuesData:ArrayCollection = new ArrayCollection();
		
		private var qccRequestCount:int;
		private var qccResponseCount:int;
		private var qccValuesData:ArrayCollection = new ArrayCollection();
		private var tempData:ArrayCollection = new ArrayCollection();
		private var spGetThirdPartyUrl:SpGetThirdPartyUrl;
		private var spUpdateBankingPartner:SpUpdateBankingPartner; 
		private var isAddBankingPartner:Boolean;
		
		/**
		 * ThirdPartyServicesManager class constructor.
		 * <br/>Handles initialize operations.
		 * @param type (IEventDispatcher)
		 * @return none
		 * @author Jerusalem Alvaira
		 */
		public function ThirdPartyServicesManager(target:IEventDispatcher=null)
		{
			initializeDatabaseProcedures();
			initializeRemoteObjects();
			initializeEventListeners();
			initializeCustomEvents();
			//super(target);
		}
		
		private function initializeDatabaseProcedures():void
		{
			spGetThirdPartyUrl = new SpGetThirdPartyUrl();
			spUpdateBankingPartner = new SpUpdateBankingPartner();
		}
		
		/**
		 * initializeRemoteObjects
		 * <br>Handles remote object 
		 * initializations. 
		 * @param none
		 * @return none
		 * @author Jerusalem Alvaira
		 */
		private function initializeRemoteObjects():void
		{
			serviceManager = new DataServiceManager();
			
			thirdPartyDataServiceManager = serviceManager.create('thirdPartyServices/ThirdPartyServices');
			
			clickMagickDataServiceManager = serviceManager.create('clickMagick/ClickMagick');
			
			qccDataServiceManager = serviceManager.create('qualityClickControl/QualityClickControl');
			
			cbDataServiceManager = serviceManager.create('clickBoss/ClickBoss');
		}
		
		private function initializeEventListeners():void
		{
			//thirdPartyDataServiceManager.getOperation("getThirdPartyUrls").addEventListener(ResultEvent.RESULT, getThirdPartyUrlsResultHandler);
			thirdPartyDataServiceManager.getOperation("updateThirdPartyValues").addEventListener(ResultEvent.RESULT, updateThirdPartyValuesResultHandler);
			
			clickMagickDataServiceManager.getOperation("getValues").addEventListener(ResultEvent.RESULT, getClickMagickValuesResultHandler);
			clickMagickDataServiceManager.getOperation("getPartnerCMValues").addEventListener(ResultEvent.RESULT, getPartnerCMValuesResultHandler);
			clickMagickDataServiceManager.getOperation("getPartnerCMValues").addEventListener(FaultEvent.FAULT, getPartnerCMValuesFaultHandler);
			
			qccDataServiceManager.getOperation("getValues").addEventListener(ResultEvent.RESULT, getQCCValuesResultHandler);
			qccDataServiceManager.getOperation("getPartnerQCCValues").addEventListener(ResultEvent.RESULT, getPartnerQCCValuesResultHandler);
			
			cbDataServiceManager.getOperation("getValues").addEventListener(ResultEvent.RESULT, getCBValuesResultHandler);
			cbDataServiceManager.getOperation("getPartnerCBValues").addEventListener(ResultEvent.RESULT, getPartnerCBValuesResultHandler);
		}
		
		protected function getPartnerCMValuesFaultHandler(event:FaultEvent):void
		{
			
		}
		
		private function initializeCustomEvents():void
		{
			spGetThirdPartyUrl.addEventListener("getThirdPartyUrlsResult", getThirdPartyUrlsResultEvent);
		}
		
		protected function getThirdPartyUrlsResultEvent(event:Event):void
		{
			var thirdPartyUrlsData:ArrayCollection = new ArrayCollection();
			var inner:Object;
			
			for(var x:String in spGetThirdPartyUrl.result.data){
				inner = new Object();
				inner = spGetThirdPartyUrl.result.data[x];
				thirdPartyUrlsData.addItem(inner);
			}
			
			dispatchEvent(new ThirdPartyServicesEvents(ThirdPartyServicesEvents.DISPLAY_THIRD_PARTY_URLS, true, false, thirdPartyUrlsData));
		}
		
		protected function getCBValuesResultHandler(event:ResultEvent):void
		{
			
		}
		
		public function getThirdPartyUrls():void
		{
			spGetThirdPartyUrl.getThirdPartyUrls();
		}
						
		public function getThirdPartyValues(data:ArrayCollection):void
		{
			for(var i:String in data){
				if(int(data[i].third_party_url_id) == 1){
					
					cmRequestCount++;
					clickMagickDataServiceManager.getValues({
						is_add_banking_partner : 0,
						receiving : (data[i].receiving == 'Y') ? 1 : 0,
						banking_partner_id : data[i].banking_partner_id,
						third_party_url : data[i].third_party_url
					});
					
				}else if(int(data[i].third_party_url_id) == 2){
					
					qccRequestCount++;
					qccDataServiceManager.getValues({
						is_add_banking_partner : 0,
						receiving : (data[i].receiving == 'Y') ? 1 : 0,
						banking_partner_id : data[i].banking_partner_id,
						third_party_url : data[i].third_party_url
					});
					
				}
			}
		}
		
		protected function getClickMagickValuesResultHandler(event:ResultEvent):void
		{
			cmResponseCount++;
			var temp:Object = new Object();
			var inner:Object;
			
			for(var i:String in event.result){	
				clickMagicValuesData.addItem(event.result[i]);
				tempData.addItem(event.result[i]); 
			}
			
			checkValues();
		}
				
		protected function getQCCValuesResultHandler(event:ResultEvent):void
		{
			qccResponseCount++;
			
			for(var i:String in event.result){	
				qccValuesData.addItem(event.result[i]);
				tempData.addItem(event.result[i]);
			}
			
			checkValues();
		}
		
		public function getPartnerThirdPartyValues(data:Object):void
		{
			isAddBankingPartner = Boolean(data.is_add_banking_partner);
			if(data.name == "QCC" || int(data.third_party_url_id) == 2){
				getPartnerQCCValues(data.third_party_url);
			}else if(data.name == "ClickMagick" || int(data.third_party_url_id) == 1){
				//getPartnerCMValues(data.third_party_url);
				getPartnerCMValues(data);
			}else if(data.name == "ClickBoss" || int(data.third_party_url_id) == 3){
				getPartnerCBValues(data.third_party_url);
			}
			
			data = null;
		}
		
		private function getPartnerCBValues(data:Object):void
		{
			cbDataServiceManager.getPartnerCBValues({third_party_url : data});
		}
		
		protected function getPartnerCBValuesResultHandler(event:ResultEvent):void
		{
			var inner:Object = new Object();
			var temp:Object = new Object();
			
			for(var i:String in event.result){
				temp = event.result[i];
				inner.total_clicks = temp.total_clicks;
				inner.unique_clicks = temp.unique_clicks;
			}
			
			dispatchEvent(new ThirdPartyServicesEvents(ThirdPartyServicesEvents.DISPLAY_CB_VALUES, true, false, inner));
		}
		
		private function getPartnerQCCValues(data:Object):void
		{
			qccDataServiceManager.getPartnerQCCValues({third_party_url : data});
		}
		
		protected function getPartnerQCCValuesResultHandler(event:ResultEvent):void
		{
			var inner:Object = new Object();
			var temp:Object = new Object();
			
			for(var i:String in event.result){
				temp = event.result[i];
				inner.total_clicks = temp.total_clicks;
				inner.unique_clicks = temp.unique_clicks;
			}
			
			/*if(!isAddBankingPartner){
				spUpdateBankingPartner.updateBankingPartnerQCCValues(inner);
			}*/
			
			dispatchEvent(new ThirdPartyServicesEvents(ThirdPartyServicesEvents.DISPLAY_QCC_VALUES, true, false, inner));
			
		}
				
		private function getPartnerCMValues(data:Object):void
		{
			//clickMagickDataServiceManager.getPartnerCMValues({third_party_url : data});
			
			clickMagickDataServiceManager.getPartnerCMValues({
				third_party_url : data.third_party_url,
				banking_partner_id : data.banking_partner_id
			});
		}
				
		protected function getPartnerCMValuesResultHandler(event:ResultEvent):void
		{
			var inner:Object = new Object();
			var temp:Object = new Object();
			
			for(var i:String in event.result){
				temp = event.result[i];
				inner.total_clicks = temp.total_clicks;
				inner.unique_clicks = temp.unique_clicks;
				inner.banking_partner_id = temp.banking_partner_id;
			}
			
			/*if(!isAddBankingPartner){
				spUpdateBankingPartner.updateBankingPartnerQCCValues(inner);
			}*/
			
			dispatchEvent(new ThirdPartyServicesEvents(ThirdPartyServicesEvents.DISPLAY_CM_VALUES, true, false, inner))
		}
		
		private function updateThirdPartyValues(data:Object):void
		{
			thirdPartyDataServiceManager.updateThirdPartyValues(data);
		}
		
		protected function updateThirdPartyValuesResultHandler(event:ResultEvent):void
		{
			
		}
		
		private function checkValues():void
		{
			if(qccRequestCount == qccResponseCount && cmRequestCount == cmResponseCount){
				dispatchEvent(new ThirdPartyServicesEvents(ThirdPartyServicesEvents.DISPLAY_THIRD_PARTY_VALUES, true, false, tempData));
			}
			
		}
	}
}