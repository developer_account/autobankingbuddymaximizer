package controller.classes
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	
	import mx.rpc.events.ResultEvent;
	import mx.rpc.remoting.mxml.RemoteObject;
	import mx.utils.ObjectUtil;
	
	import scripts.events.ComponentEvents;
	import scripts.events.LoginEvents;
	import scripts.events.ThirdPartyServicesEvents;
	import scripts.lib.DataServiceManager;
	import scripts.lib.SessionManager;
	import scripts.sqlProcedures.SpGetBankingPartner;
	import scripts.sqlProcedures.SpGetUserAuthentication;
	import scripts.sqlProcedures.SpUpdateBankingPartner;
	import scripts.sqlProcedures.SpUpdateUserAuthentication;
	
	import view.Login;
	
	public class LoginManager extends EventDispatcher
	{
		private var serviceManager:DataServiceManager;
		private var thirdPartyServicesManager:ThirdPartyServicesManager = new ThirdPartyServicesManager();
		
		private var userAuthenticationDataServiceManager:RemoteObject;
		private var licenseDataServiceManager:RemoteObject;
		private var dapDataServiceManager:RemoteObject;
		private var lastLoggedIn:String;
		private var userData:Object;
		private var spGetBankingPartner:SpGetBankingPartner = new SpGetBankingPartner();
		private var spUpdateUserAuthentication:SpUpdateUserAuthentication = new SpUpdateUserAuthentication();
		private var spGetUserAuthentication:SpGetUserAuthentication = new SpGetUserAuthentication();
		private var userRegistrationData:Object;
		private var validUser:Boolean;
		private var validLicense:Boolean;
		private var userRegistration:Boolean;
		private var requestValidationCount:int;
		
		public function LoginManager(target:IEventDispatcher=null)
		{
			//TODO: implement function
			//super(target);
			initializeDatabaseProcedures();
			initializeRemoteObjects();
			initializeEventListeners();
			initializeCustomEvents();
			
		}
		
		private function initializeDatabaseProcedures():void
		{
			spGetBankingPartner = new SpGetBankingPartner();
		}
		
		private function initializeCustomEvents():void
		{
			thirdPartyServicesManager.addEventListener(ThirdPartyServicesEvents.LOAD_LOGIN, loadLoginEvent);
			spGetUserAuthentication.addEventListener("getResult", getResultEvent);	
			spGetUserAuthentication.addEventListener("getAuthenticationResult", getAuthenticationResultEvent);
			spGetUserAuthentication.addEventListener("getAuthenticationResultError", getAuthenticationResultErrorEvent);
			
			spUpdateUserAuthentication.addEventListener("registerUserResult", registerUserResultEvent);
		}
		
		protected function loadLoginEvent(event:Event):void
		{
			dispatchEvent(new LoginEvents(LoginEvents.LOGIN_SUCCESS, true, false, null));
		}
		
		private function initializeEventListeners():void
		{
			//userAuthenticationDataServiceManager.getOperation('getLicenseVerification').addEventListener(ResultEvent.RESULT, getLicenseVerificationResultHandler);
			//userAuthenticationDataServiceManager.getOperation('authenticateUser').addEventListener(ResultEvent.RESULT, authenticateUserResultHandler);
			userAuthenticationDataServiceManager.getOperation('registerUser').addEventListener(ResultEvent.RESULT, registerUserResultHandler);
			
			licenseDataServiceManager.getOperation('getlicenseValidity').addEventListener(ResultEvent.RESULT, getlicenseValidityResultHandler);
			
			dapDataServiceManager.getOperation('authenticateUser').addEventListener(ResultEvent.RESULT, authenticateUserResultHandler);
			
		}
		
		
		private function initializeRemoteObjects():void
		{
			serviceManager = new DataServiceManager();
			
			userAuthenticationDataServiceManager = serviceManager.create('authentication/Authentication');
			
			licenseDataServiceManager = serviceManager.createLicense('licensing/Licensing');
			
			dapDataServiceManager = serviceManager.createLicense('authentication/Authentication');
		}
		
		public function authenticateUser(data:Object):void{
			spGetUserAuthentication.authenticateUser(data);
		}
				
		private function checkLicenseValidity(data:Object):void
		{
			if(userRegistration){
				
				licenseDataServiceManager.getlicenseValidity({
					mode:3,
					license:data.license,
					username:userRegistrationData.user_name
				});
				
			}else{
				
				licenseDataServiceManager.getlicenseValidity({
					mode:2,
					license:data.license
				});
				
			}
		}
		
		protected function getlicenseValidityResultHandler(event:ResultEvent):void
		{
			var status:int = 0;
			
			if(event.result != null){
				
				if(userRegistration){
					
					for(var i:String in event.result){
						
						if(event.result[i].hasOwnProperty('status')){
						
							trace('validLicense: '+validLicense);
							
							if(int(event.result[0].status) > 0){
								validLicense = true;
									
							}else{
									
								validLicense = false;
									
							}
							
						}else{
							
							validLicense = false;
							
						}
							
					}
					
					requestValidationCount++;
					validateUser();
					
				}else{
					
					for(var j:String in event.result){
						if(event.result[j].hasOwnProperty('status')){
							status = event.result[j].status;
						}
					}
					
					dispatchEvent(new LoginEvents(LoginEvents.LOGIN_SUCCESS, true, false, 
						{
							status : status,
							user_data : userData
						}));	
					
				}
				
			}
			
		}
		
		public function registerUser(data:Object):void
		{
			userRegistrationData = new Object;
			userRegistrationData = data;
			
			userRegistration = true;
			
			checkUserValidity(data);
			checkLicenseValidity(data);
			
			data = null;
		}
		
		private function checkUserValidity(data:Object):void
		{
			dapDataServiceManager.authenticateUser({
				mode : 1,
				user_name : data.user_name,
				password : data.password
			});
		}
		
		protected function authenticateUserResultHandler(event:ResultEvent):void
		{
			if(String(event.result[0].user_product_status) == 'A' || String(event.result[0].user_product_status) != null){
				validUser = true;
			}else{
				validUser = false;
			}
			
			requestValidationCount++;
			validateUser();
			
		}
		
		private function validateUser():void
		{
			trace('requestValidationCount: '+requestValidationCount);
			if(requestValidationCount == 2){
				
				trace(validLicense);
				trace(validUser);
				if(validLicense && validUser){
					
					dispatchEvent(new LoginEvents(LoginEvents.REGISTER_USER, true, false, {register : true}));
					
					validLicense = false;
					validUser = false;
					userRegistration = false;
					
				}else{
					
					dispatchEvent(new LoginEvents(LoginEvents.REGISTER_USER, true, false, {register : false}));
					
				}
				
				requestValidationCount = 0;
				
			}
		}
		
		public function searchDuplicates():void
		{
			spUpdateUserAuthentication.searchDuplicates(userRegistrationData);
		}
		
		protected function registerUserResultEvent(event:Event):void
		{
			dispatchEvent(new LoginEvents(LoginEvents.REGISTRATION_SUCCESS, true, false, spUpdateUserAuthentication.result.data[0].success));
		}
		
		protected function registerUserResultHandler(event:ResultEvent):void
		{
			dispatchEvent(new LoginEvents(LoginEvents.REGISTRATION_SUCCESS, true, false, event.result[0]));
		}
		
		protected function getLicenseVerificationResultHandler(event:ResultEvent):void
		{
			
		}
				
		public function getUserCount():void
		{
			
			spGetUserAuthentication.getUserCount();
			
		}
		
		protected function getResultEvent(event:Event):void
		{
			dispatchEvent(new LoginEvents(LoginEvents.GET_USER_COUNT_RESULT, true, false, spGetUserAuthentication.result.data[0].user_count));
		}
		
		protected function getAuthenticationResultEvent(event:Event):void
		{
			userData = new Object();
			
			userData = spGetUserAuthentication.result.data;
			
			if(spGetUserAuthentication.result.data.length > 0){
				checkLicenseValidity({license : spGetUserAuthentication.result.data[0].license});
				lastLoggedIn = String(spGetUserAuthentication.result.data[0].last_logged_in);
				
			}
			
		}
		
		protected function getAuthenticationResultErrorEvent(event:Event):void
		{
			dispatchEvent(new LoginEvents(LoginEvents.LOGIN_SUCCESS, true, false, 
				{
					status : 0,
					user_data : ''
				}));
		}
	}
}