package controller.classes
{
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	
	import mx.rpc.events.ResultEvent;
	import mx.rpc.remoting.mxml.RemoteObject;
	import mx.utils.ObjectUtil;
	
	import scripts.events.ClicksProjectionsEvents;
	import scripts.lib.DataServiceManager;
	
	public class ClicksProjectionsManager extends EventDispatcher
	{
		private var serviceManager:DataServiceManager;
		private var clicksProjectionsDataServiceManager:RemoteObject;
		
		public function ClicksProjectionsManager(target:IEventDispatcher=null)
		{
			//super(target);
			
			initializeRemoteObjects();
			initializeEventListeners();
		}
		
		private function initializeRemoteObjects():void
		{
			serviceManager = new DataServiceManager();
			
			clicksProjectionsDataServiceManager = serviceManager.create('clicksProjections/ClicksProjections');
		}
		
		private function initializeEventListeners():void
		{
			clicksProjectionsDataServiceManager.getOperation("getClicksProjections").addEventListener(ResultEvent.RESULT, getClicksProjectionsResultHandler); 
		}
		
		public function getClicksProjections():void
		{
			clicksProjectionsDataServiceManager.getClicksProjections({mode : 1});
		}
		
		protected function getClicksProjectionsResultHandler(event:ResultEvent):void
		{
			dispatchEvent(new ClicksProjectionsEvents(ClicksProjectionsEvents.DISPLAY_CLICKS_PROJECTIONS, true, false, {
				average_opt_in_rate : event.result[0].average_opt_in_rate, 
				clicks_coming_in : event.result[0].clicks_coming_in
			}));
			
		}		
		
		public function unload():void
		{
			serviceManager.destroy(clicksProjectionsDataServiceManager);
		}
	}
}