package controller.classes
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	
	import model.dataobjects.BankingPartnersData;
	
	import mx.collections.ArrayCollection;
	import mx.formatters.DateFormatter;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.remoting.mxml.RemoteObject;
	import mx.utils.ObjectUtil;
	import mx.utils.StringUtil;
	
	import scripts.events.BankingPartnersEvents;
	import scripts.events.ThirdPartyServicesEvents;
	import scripts.lib.DataServiceManager;
	import scripts.sqlProcedures.SpGetBankingPartner;
	import scripts.sqlProcedures.SpUpdateBankingPartner;
	
	public class BankingPartnersManager extends EventDispatcher
	{
		
		include "../../scripts/formatters/GlobalFormatter.as";
		
		private var serviceManager:DataServiceManager;
		private var bankingPartnersDataServiceManager:RemoteObject;
		private var requestCount:int;
		private var receivingBankingPartnersData:ArrayCollection;
		private var sendingBankingPartnersData:ArrayCollection;
		private var resultCount:int;
		private var thirdPartyServicesManager:ThirdPartyServicesManager = new ThirdPartyServicesManager();
		
		private var newSendingBankingPartnersData:ArrayCollection = new ArrayCollection();
		private var newReceivingBankingPartnersData:ArrayCollection = new ArrayCollection();
		private var receivingRequestCount:int;
		private var receivingResultCount:int;
		private var thirdPartyUrlsList:ArrayCollection;
		private var spGetBankingPartner:SpGetBankingPartner;
		private var spUpdateBankingPartner:SpUpdateBankingPartner;

		public function BankingPartnersManager(target:IEventDispatcher=null)
		{
			initializeDatabaseProcedures();
			initializeRemoteObjects();
			initializeEventListeners();
			initializeCustomEvents();
			//removeRemoteObjects();
			//removeEventListeners();
		}
		
		private function initializeDatabaseProcedures():void
		{
			spGetBankingPartner = new SpGetBankingPartner();
			spUpdateBankingPartner = new SpUpdateBankingPartner();
		}
		
		private function initializeRemoteObjects():void
		{
			serviceManager = new DataServiceManager();
			
			bankingPartnersDataServiceManager = serviceManager.create('bankingPartners/BankingPartners');
			
		}
		
		private function initializeEventListeners():void
		{
			bankingPartnersDataServiceManager.getOperation('updateBankingPartnerCMValues');	
		}
		
		private function initializeCustomEvents():void
		{
			thirdPartyServicesManager.addEventListener(ThirdPartyServicesEvents.DISPLAY_THIRD_PARTY_VALUES, displayThirdPartyValuesEvent);
			
			thirdPartyServicesManager.addEventListener(ThirdPartyServicesEvents.DISPLAY_QCC_VALUES, displayQCCValuesEvent);
			thirdPartyServicesManager.addEventListener(ThirdPartyServicesEvents.DISPLAY_CM_VALUES, displayCMValuesEvent);
			thirdPartyServicesManager.addEventListener(ThirdPartyServicesEvents.DISPLAY_CB_VALUES, displayCBValuesEvent);
			
			spGetBankingPartner.addEventListener("getTopReceivingBankingPartnersResult", getTopReceivingBankingPartnersResultEvent);
			spGetBankingPartner.addEventListener("getTopSendingBankingPartnersResult", getTopSendingBankingPartnersResultEvent);
			spGetBankingPartner.addEventListener("getBankingPartnersListResult", getBankingPartnersListResultEvent);
			spGetBankingPartner.addEventListener("getReceivingBankingPartnersResult", getReceivingBankingPartnersResultEvent);
			spGetBankingPartner.addEventListener("getSendingBankingPartnersResult", getSendingBankingPartnersResultEvent);
			spGetBankingPartner.addEventListener("getReceivingChartDataResult", getReceivingChartDataResultEvent);
			spGetBankingPartner.addEventListener("getSendingChartDataResult", getSendingChartDataResultEvent);
			
			spUpdateBankingPartner.addEventListener("addBankingPartnerResult", addBankingPartnerResultEvent);
			spUpdateBankingPartner.addEventListener("editBankingPartnerResult", editBankingPartnerResultEvent);
			spUpdateBankingPartner.addEventListener("deleteBankingPartnerResult", deleteBankingPartnerResultEvent);
			
		}
		
		protected function displayCBValuesEvent(event:ThirdPartyServicesEvents):void
		{
			trace('CB: '+ObjectUtil.toString(event.data));
		}
		
		protected function displayCMValuesEvent(event:ThirdPartyServicesEvents):void
		{
			spUpdateBankingPartner.updateBankingPartnerThirdPartyValues(event.data);
		}
		
		protected function displayQCCValuesEvent(event:ThirdPartyServicesEvents):void
		{
			trace('QCC: '+ObjectUtil.toString(event.data));
		}
		
		protected function editBankingPartnerResultEvent(event:Event):void
		{
			dispatchEvent(new BankingPartnersEvents(BankingPartnersEvents.ADD_BANKING_PARTNER_RESULT, true, false, spUpdateBankingPartner.success));
		}
		
		protected function displayThirdPartyUrlsEvent(event:ThirdPartyServicesEvents):void
		{
			thirdPartyUrlsList = new ArrayCollection();
			
			thirdPartyUrlsList = event.data as ArrayCollection;
		}
				
		protected function updateBankingPartnerCMValuesResultHandler(event:ResultEvent):void
		{
			
		}
		
		public function getReceivingBankingPartners():void
		{
			spGetBankingPartner.getBankingPartners({receiving:1});
		}
		
		protected function getReceivingBankingPartnersResultEvent(event:Event):void
		{
			var receivingBankingPartnersData:ArrayCollection = new ArrayCollection();
			var clicks:Number = new Number();
			var inner:Object;
			var temp:Object = new Object();
			
			for(var i:String in spGetBankingPartner.result.data){
				/*if(int(event.result[i].third_party_url_id) > 0){
				clicks += int(event.result[i].unique_clicks);
				}else{*/
				clicks += int(spGetBankingPartner.result.data[i].clicks_owed);
				//}
			}
			
			for(var x:String in spGetBankingPartner.result.data){
				inner = new Object();
				temp = spGetBankingPartner.result.data[x];
					
				inner.fullDetails = 
					{
						receiving : (temp.receiving > 0) ? 'Y' : 'N',
							banking_partner_id : temp.banking_partner_id,
							banking_partner_name : temp.first_name+' '+temp.last_name,
							//clicks_owed : (int(temp.third_party_url_id) > 0) ? temp.unique_clicks : temp.clicks_owed,
							clicks_owed : temp.clicks_owed,
							total_clicks : 0,
							is_total : false
					}
				
				receivingBankingPartnersData.addItem(inner);
			}
			
			inner = new Object();
			
			inner.fullDetails = 
				{
					receiving : 0,
					banking_partner_id : '',
					banking_partner_name : 'Total',
					clicks_owed : 0,
					total_clicks : clicks,
					is_total : true
				}
			receivingBankingPartnersData.addItem(inner);
			
			dispatchEvent(new BankingPartnersEvents(BankingPartnersEvents.DISPLAY_RECEIVING_BANKING_PARTNERS, true, false, receivingBankingPartnersData));
		}
		
		public function getSendingBankingPartners():void
		{
			spGetBankingPartner.getBankingPartners({receiving:2});
		}
		
		protected function getSendingBankingPartnersResultEvent(event:Event):void
		{
			
			var sendingBankingPartnersData:ArrayCollection = new ArrayCollection();
			var clicks:Number = new Number();
			var inner:Object;
			var temp:Object = new Object();
			
			for(var i:String in spGetBankingPartner.result.data){
				/*if(int(event.result[i].third_party_url_id) > 0){
				clicks += int(event.result[i].unique_clicks);
				}else{*/
				clicks += int(spGetBankingPartner.result.data[i].clicks_owed);
				//}
			}
			
			for(var x:String in spGetBankingPartner.result.data){
				inner = new Object();
				temp = spGetBankingPartner.result.data[x];
				
				inner.fullDetails = 
					{
						receiving : (temp.receiving > 0) ? 'Y' : 'N',
							banking_partner_id : temp.banking_partner_id,
							banking_partner_name : temp.first_name+' '+temp.last_name,
							//clicks_owed : (int(temp.third_party_url_id) > 0) ? temp.unique_clicks : temp.clicks_owed,
							clicks_owed : temp.clicks_owed,
							total_clicks : 0,
							is_total : false
					}
				
				sendingBankingPartnersData.addItem(inner);
			}
			
			inner = new Object();
			
			inner = new Object();
			
			inner.fullDetails = 
				{
					receiving : 0,
					banking_partner_id : '',
					banking_partner_name : 'Total',
					clicks_owed : 0,
					total_clicks : clicks,
					is_total : true
				}
			
			sendingBankingPartnersData.addItem(inner);
			
			dispatchEvent(new BankingPartnersEvents(BankingPartnersEvents.DISPLAY_SENDING_BANKING_PARTNERS, true, false, sendingBankingPartnersData));
			
		}
		
		public function getBankingPartnersList():void
		{
			spGetBankingPartner.getBankingPartnersList()
		}
		
		protected function getBankingPartnersListResultEvent(event:Event):void
		{
			
			var bankingPartnersListData:ArrayCollection = new ArrayCollection();
			var inner:Object;
			var temp:Object = new Object();
			var df:DateFormatter = new DateFormatter();
			df.formatString = 'M/DD/YYYY';
			
			for(var x:String in spGetBankingPartner.bankingPartnersList){
				temp = spGetBankingPartner.bankingPartnersList[x];
				inner = new Object();
				
				var receiving:String = '';
				var receiving_index:int = 0;
				var uniqueClicks:int = 0;
				var completed:Boolean = true;
				
				if(int(temp.receiving) == 0){
					receiving = 'Swap';
					receiving_index = 2;
				}else if(int(temp.receiving) == 1){
					receiving = 'N';
					receiving_index = 1;
				}else if(int(temp.receiving) == 2){
					receiving = 'Y';
					receiving_index = 0;
				}
				
				if(temp.unique_clicks == '-'){
					uniqueClicks = 0;
				}else{
					uniqueClicks = temp.unique_clicks;
				}
				
				if(uniqueClicks > int(temp.clicks_owed)){
					completed = true;
				}else{
					completed = false;
				}
				
				inner.fullDetails = {
					receiving : receiving,
					receiving_index : receiving_index,
					sent_first : (temp.sent_first > 0) ? 'Y' : 'N',
					sent_first_index : (temp.sent_first > 0) ? 0 : 1,
					total_clicks : temp.total_clicks,
					unique_clicks : temp.unique_clicks,
					date_started : df.format(temp.date_started),
					banking_partner_id : temp.banking_partner_id, 
					first_name : temp.first_name,
					last_name : temp.last_name,
					banking_partner_name : temp.first_name+' '+temp.last_name,
					third_party_url_id : temp.third_party_url_id,
					third_party_url : temp.third_party_url,
					third_party_url_name : temp.third_party_url_name,
					click_value_type : (int(temp.third_party_url_id) > 0) ? "URL" : "Manually Enter Clicks",
					number_of_rounds : temp.number_of_rounds,
					clicks_owed : temp.clicks_owed,
					status : temp.status,
					//completed : {clicks_owed : temp.clicks_owed, unique_clicks : temp.unique_clicks}
					completed : completed
				}
				
				bankingPartnersListData.addItem(inner);
			}
			
			dispatchEvent(new BankingPartnersEvents(BankingPartnersEvents.DISPLAY_BANKING_PARTNERS_LIST, true, false, bankingPartnersListData));
		}
		
		public function getTopReceivingBankingPartners():void
		{
			spGetBankingPartner.getTopBankingPartners({receiving:1});	
		}
		
		protected function getTopReceivingBankingPartnersResultEvent(event:Event):void
		{
			var receivingBankingPartnersData:ArrayCollection = new ArrayCollection();
			var clicks:Number = new Number();
			var clicksOwed:Number = new Number();
			var inner:Object;
			var temp:Object = new Object();
			
			for(var x:String in spGetBankingPartner.result.data){

				var uniqueClicks:int = 0;
				var completed:Boolean = true;
				
				inner = new Object();
				temp = spGetBankingPartner.result.data[x];
				
				if(temp.third_party_url_id > 0){
					thirdPartyServicesManager.getPartnerThirdPartyValues({
						third_party_url_id : temp.third_party_url_id,
						third_party_url : temp.third_party_url,
						banking_partner_id : temp.banking_partner_id
					});
				}
				
				if(temp.unique_clicks == '-'){
					uniqueClicks = 0;
				}else{
					uniqueClicks = temp.unique_clicks;
				}
				
				if(uniqueClicks > int(temp.clicks_owed)){
					completed = true;
				}else{
					completed = false;
				}
				
				
				if(!completed){
					clicksOwed += Math.abs(int(temp.clicks_owed)-int(temp.unique_clicks));
					inner.fullDetails = 
						{
							receiving : (temp.receiving > 0) ? 'Y' : 'N',
								banking_partner_id : temp.banking_partner_id,
								banking_partner_name : temp.first_name+' '+temp.last_name,
								//clicks_owed : (int(temp.third_party_url_id) > 0) ? StringUtil.trim(temp.unique_clicks) : StringUtil.trim(temp.clicks_owed),
								clicks_owed : Math.abs(int(temp.clicks_owed)-int(temp.unique_clicks)), 
								total_clicks : 0,
								is_total : false
						}
					
					receivingBankingPartnersData.addItem(inner);
				}
			}
			
			inner = new Object();
			
			inner = new Object();
			
			inner.fullDetails = 
				{
					receiving : 0,
					banking_partner_id : '',
					banking_partner_name : 'Total',
					clicks_owed : 0,
					//total_clicks : clicks,
					total_clicks : clicksOwed,
					is_total : true
				}
			
			receivingBankingPartnersData.addItem(inner);
			
			dispatchEvent(new BankingPartnersEvents(BankingPartnersEvents.DISPLAY_TOP_RECEIVING_BANKING_PARTNERS, true, false, receivingBankingPartnersData));
		}
		
		public function getTopSendingBankingPartners():void
		{
			requestCount++;
			spGetBankingPartner.getTopBankingPartners({receiving:2});
		}
		
		protected function getTopSendingBankingPartnersResultEvent(event:Event):void
		{
			var sendingBankingPartnersData:ArrayCollection = new ArrayCollection();
			var clicks:Number = new Number();
			var clicksOwed:Number = new Number();
			var inner:Object;
			var temp:Object = new Object();
						
			for(var x:String in spGetBankingPartner.result.data){
				var uniqueClicks:int = 0;
				var completed:Boolean = true;
				
				inner = new Object();
				temp = spGetBankingPartner.result.data[x];
				
				if(temp.third_party_url_id > 0){
					thirdPartyServicesManager.getPartnerThirdPartyValues({
						third_party_url_id : temp.third_party_url_id,
						third_party_url : temp.third_party_url,
						banking_partner_id : temp.banking_partner_id
					});
				}
				
				if(temp.unique_clicks == '-'){
					uniqueClicks = 0;
				}else{
					uniqueClicks = temp.unique_clicks;
				}
				
				if(uniqueClicks > int(temp.clicks_owed)){
					completed = true;
				}else{
					completed = false;
				}
				
				if(!completed){
					clicksOwed += Math.abs(int(temp.clicks_owed)-int(temp.unique_clicks));
					inner.fullDetails = 
						{
							receiving : (temp.receiving > 0) ? 'Y' : 'N',
								banking_partner_id : temp.banking_partner_id,
								banking_partner_name : temp.first_name+' '+temp.last_name,
								//clicks_owed : (int(temp.third_party_url_id) > 0) ? temp.unique_clicks : temp.clicks_owed,
								clicks_owed : Math.abs(int(temp.clicks_owed)-int(temp.unique_clicks)),
								total_clicks : 0,
								is_total : false
						}
					
					sendingBankingPartnersData.addItem(inner);
				}
			}
			
			inner = new Object();
			
			inner = new Object();
			
			inner.fullDetails = 
				{
					receiving : 0,
					banking_partner_id : '',
					banking_partner_name : 'Total',
					clicks_owed : 0,
					//total_clicks : clicks,
					total_clicks : clicksOwed,
					is_total : true
				}
			
			sendingBankingPartnersData.addItem(inner);
			
			dispatchEvent(new BankingPartnersEvents(BankingPartnersEvents.DISPLAY_TOP_SENDING_BANKING_PARTNERS, true, false, sendingBankingPartnersData));
		}
		
		public function getTopReceivingChartData():void
		{
			spGetBankingPartner.getChartData({receiving:1});
		}
				
		protected function getReceivingChartDataResultEvent(event:Event):void
		{
			if(spGetBankingPartner.receivingChartData != null){
				dispatchEvent(new BankingPartnersEvents(BankingPartnersEvents.DISPLAY_RECEIVING_CHART_DATA, true, false, spGetBankingPartner.receivingChartData));
			}
		}
		
		public function getTopSendingChartData():void
		{
			spGetBankingPartner.getChartData({receiving:2});
		}
		
		protected function getSendingChartDataResultEvent(event:Event):void
		{
			if(spGetBankingPartner.sendingChartData != null){
				dispatchEvent(new BankingPartnersEvents(BankingPartnersEvents.DISPLAY_SENDING_CHART_DATA, true, false, spGetBankingPartner.sendingChartData));
			}
		}
		
		/*public function getChartData():void
		{
			spGetBankingPartner.getChartData();
		}
		
		protected function getChartDataResultEvent(event:Event):void
		{
			if(spGetBankingPartner.chartData != null){
				if(spGetBankingPartner.chartData.length > 0){
					dispatchEvent(new BankingPartnersEvents(BankingPartnersEvents.DISPLAY_CHART_DATA, true, false, spGetBankingPartner.chartData));
				}
			}
		}*/
		
		private function getThirdPartyValues(data:ArrayCollection, type:String):void
		{
			if(type == 'sending'){
				if(requestCount == resultCount){
					requestCount = 0;
					resultCount = 0;
					newSendingBankingPartnersData = new ArrayCollection();
					thirdPartyServicesManager.getThirdPartyValues(data);
					
				}
			}else{
				if(receivingRequestCount == receivingResultCount){
					receivingRequestCount = 0;
					receivingResultCount = 0;
					newReceivingBankingPartnersData = new ArrayCollection();
					thirdPartyServicesManager.getThirdPartyValues(data);
					
				}
			}
		}
		
		public function addBankingPartner(data:Object):void
		{
			spUpdateBankingPartner.addBankingPartner(data);
			data = null;
		}
		
		protected function addBankingPartnerResultEvent(event:Event):void
		{
			dispatchEvent(new BankingPartnersEvents(BankingPartnersEvents.ADD_BANKING_PARTNER_RESULT, true, false, spUpdateBankingPartner.result.data[0].success));
		}
		
		public function editBankingPartner(data:Object):void
		{
			spUpdateBankingPartner.editBankingPartner(data);
			data = null;
		}
		
		private function removeRemoteObjects():void
		{
			bankingPartnersDataServiceManager = null;
		}
		
		public function deleteBankingPartners(data:Object):void
		{
			spUpdateBankingPartner.deleteBankingPartners(data);
			data = null;
		}
		
		protected function deleteBankingPartnerResultEvent(event:Event):void
		{
			dispatchEvent(new BankingPartnersEvents(BankingPartnersEvents.DELETE_BANKING_PARTNER_RESULT, true, false, spUpdateBankingPartner.success));
		}
		
		protected function displayThirdPartyValuesEvent(event:ThirdPartyServicesEvents):void
		{
			var thirdPartyValues:ArrayCollection = new ArrayCollection();
			thirdPartyValues = event.data as ArrayCollection;
			
			var innerThirdPartyValues:Object = new Object();
			var tempThirdPartyValues:Object = new Object();
			
			var innerSendingBankingPartnersData:Object = new Object();
			var tempSendingBankingPartnersData:Object = new Object();
			
			var innerReceivingBankingPartnersData:Object = new Object();
			var tempReceivingBankingPartnersData:Object = new Object();
			
			var inner:Object;		
			
			for(var i:String in sendingBankingPartnersData){
				innerSendingBankingPartnersData = sendingBankingPartnersData[i];
				
				for(var j:String in thirdPartyValues){
					if(int(innerSendingBankingPartnersData.banking_partner_id) == int(thirdPartyValues[j].banking_partner_id)){
						inner = new Object();
						tempThirdPartyValues = thirdPartyValues[j];
						inner.fullDetails = {
							banking_partner_id : tempThirdPartyValues.banking_partner_id,
							receiving : innerSendingBankingPartnersData.receiving,
							banking_partner_name : innerSendingBankingPartnersData.banking_partner_name,
							clicks_owed : (int(innerSendingBankingPartnersData.third_party_url_id) > 0) ? 
											tempThirdPartyValues.unique_clicks :
											innerSendingBankingPartnersData.clicks_owed,
							third_party_url : innerSendingBankingPartnersData.third_party_url,
							third_party_url_id : innerSendingBankingPartnersData.third_party_url_id,
							total_clicks : tempThirdPartyValues.total_clicks,
							unique_clicks : tempThirdPartyValues.unique_clicks
						}
						
						if(int(tempThirdPartyValues.receiving) == 0){
							newSendingBankingPartnersData.addItem(inner);					
						}
						
					}else{
						inner = new Object();
						
						inner.fullDetails = {
							banking_partner_id : innerSendingBankingPartnersData.banking_partner_id,
							receiving : innerSendingBankingPartnersData.receiving,
							banking_partner_name : innerSendingBankingPartnersData.banking_partner_name,
							clicks_owed : innerSendingBankingPartnersData.clicks_owed,
							third_party_url : innerSendingBankingPartnersData.third_party_url,
							third_party_url_id : innerSendingBankingPartnersData.third_party_url_id,
							total_clicks : innerSendingBankingPartnersData.total_clicks,
							unique_clicks : innerSendingBankingPartnersData.unique_clicks
						}
							
						//if(int(innerSendingBankingPartnersData.receiving) == 0){
							newSendingBankingPartnersData.addItem(inner);
						//}
												
					}
				}
			}
			
			for(var k:String in receivingBankingPartnersData){
				innerReceivingBankingPartnersData = receivingBankingPartnersData[k];
				
				for(var l:String in thirdPartyValues){
					if(int(innerReceivingBankingPartnersData.banking_partner_id) == int(thirdPartyValues[l].banking_partner_id)){
						inner = new Object();
						tempThirdPartyValues = thirdPartyValues[l];
						inner.fullDetails = {
							banking_partner_id : tempThirdPartyValues.banking_partner_id,
							receiving : innerReceivingBankingPartnersData.receiving,
							banking_partner_name : innerReceivingBankingPartnersData.banking_partner_name,
							clicks_owed : (int(innerReceivingBankingPartnersData.third_party_url_id) > 0) ? 
											tempThirdPartyValues.unique_clicks :
											innerReceivingBankingPartnersData.clicks_owed,
							third_party_url : innerReceivingBankingPartnersData.third_party_url,
							third_party_url_id : innerReceivingBankingPartnersData.third_party_url_id,
							total_clicks : tempThirdPartyValues.total_clicks,
							unique_clicks : tempThirdPartyValues.unique_clicks
						}
						
						if(int(tempThirdPartyValues.receiving) > 0){
							newReceivingBankingPartnersData.addItem(inner);
						}
						
					}
				}
			}
			
			dispatchEvent(new BankingPartnersEvents(BankingPartnersEvents.DISPLAY_TOP_RECEIVING_BANKING_PARTNERS, true, false, newReceivingBankingPartnersData));
			dispatchEvent(new BankingPartnersEvents(BankingPartnersEvents.DISPLAY_TOP_SENDING_BANKING_PARTNERS, true, false, newSendingBankingPartnersData));
			
		}
				
		public function unload():void
		{
			serviceManager.destroy(bankingPartnersDataServiceManager);
			removeEventListeners();
			removeRemoteObjects();
			initializeRemoteObjects();
			initializeEventListeners();
			initializeCustomEvents();
			
		}
		
		public function removeEventListeners():void
		{
			thirdPartyServicesManager.removeEventListener(ThirdPartyServicesEvents.DISPLAY_THIRD_PARTY_VALUES, displayThirdPartyValuesEvent);
			
			spGetBankingPartner.removeEventListener("getTopReceivingBankingPartnersResult", getTopReceivingBankingPartnersResultEvent);
			spGetBankingPartner.removeEventListener("getTopSendingBankingPartnersResult", getTopSendingBankingPartnersResultEvent);
			spGetBankingPartner.removeEventListener("getBankingPartnersListResult", getBankingPartnersListResultEvent);
			spGetBankingPartner.removeEventListener("getReceivingBankingPartnersResult", getReceivingBankingPartnersResultEvent);
			spGetBankingPartner.removeEventListener("getSendingBankingPartnersResult", getSendingBankingPartnersResultEvent);
			
			spUpdateBankingPartner.removeEventListener("addBankingPartnerResult", addBankingPartnerResultEvent);
			spUpdateBankingPartner.removeEventListener("editBankingPartnerResult", editBankingPartnerResultEvent);
			spUpdateBankingPartner.removeEventListener("deleteBankingPartnerResult", deleteBankingPartnerResultEvent);
			
		}
		
	}
}